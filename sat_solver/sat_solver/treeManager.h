#ifndef __TREE__MANAGER__
#define __TREE__MANAGER__
#include <iostream>
#include <vector>
#include <assert.h>

#include "clauseManager.h"

#define LEFT_BRANCH 0
#define RIGHT_BRANCH 1
#define TAIL 1
#define FORWARD 1
#define BACKWORD 0

using namespace std;

struct clsNode {
	int val;
	bool is_root;
	bool is_marked;
	clsNode* temp_link;
	clsNode* parent_node;
	vector<clsNode*> child_nodes;
};

typedef vector<clsNode*>::iterator nodeLoc;
typedef vector<int>::const_iterator intLoc;

class treeManager{
private:
	clauseManager& cls_manager;
	vector<serializeClause> _result_cls;
	vector<int> _path;
	vector<int> _material;
	clsNode* _root;
	clsNode *_p_node, *_n_node;
	clsNode* _spliting_point;
	int _abandon_count, _insert_count, _prune_count, _prune_opposite;
	int _prune_p, _prune_n;
	int _cls_count;

	//初次建立树
	clsNode* _create_node(clsNode*, int);
	clsNode* _nevigate(clsNode*, int);
	nodeLoc _loc_nevigate(clsNode*, int);
	void _build_branch(const sCls&);
	void _build_search_tree();
	//分离
	void _reset();
	//相乘
	void _record(clsNode*);
	bool _check(const vector<int>&);
	void _multiply(clsNode*);
	void _count_cls(clsNode*);
	//删除子树
	void _destory(clsNode*);
	//记录结果
	void _dump(vector<sCls>&, clsNode*);
	//优化尝试
		//插入和剪枝
	void _insert_clause(const sCls&);
	nodeLoc _locate_branch(vector<clsNode*>&, int);
		//提前消除
	clsNode* _nevigate(clsNode*, intLoc);
	nodeLoc _loc_nevigate(clsNode*, intLoc);
	bool _verify_sub_cls(clsNode*, intLoc, intLoc);
	void _delete_path(clsNode*, intLoc, intLoc);
public:

	treeManager();
	void set_up();
	//运算时使用
	bool build();
	void multiply();
	void collect();
	void destroy();

	int count_clause();
	void dump();
	void dump_new_cls();
	~treeManager();
};

#endif