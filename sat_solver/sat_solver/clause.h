#ifndef __CLAUSE__
#define __CLAUSE__

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class clause {
protected:
	int _length;
	vector<int> _cls_vars; //����

public:
	clause();
	clause(const vector<int>&);
	clause(const clause&);
	const vector<int>& get_literals() const;
	int length()const;
	
	~clause();
};

typedef clause Cls;

#endif // !__CLAUSE__