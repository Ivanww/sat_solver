#ifndef __SERIALIZE_CLAUSE__
#define __SERIALIZE_CLAUSE__

#include <iostream>
#include <vector>

#include "clause.h"

using namespace std;

enum valType {
	ORI_VAL = 1,
	TRANS_VAL = 2
};

class serializeClause : public clause{
public:

	serializeClause();
	serializeClause(const vector<int>&, valType);
	serializeClause(const clause&);
	serializeClause(const serializeClause&);

	int first_var() const;
	bool compare(const serializeClause&) const;
	bool operator<(const serializeClause&);
	bool operator>(const serializeClause&);
	bool operator==(const serializeClause&);

	~serializeClause();

private:
	int _first_var;

	void _trans_vars();
	void _sort_vars();
};

typedef serializeClause sCls;

#endif