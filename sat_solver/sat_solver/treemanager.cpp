#include "treeManager.h"

treeManager::treeManager(): cls_manager(clauseManager::get_instance()){
	_root = new clsNode;
	_root->is_root = true;
	_root->val = 0;
	_root->temp_link = NULL;
	_root->is_marked = false;
	_root->parent_node = NULL;
	_p_node = NULL;
	_n_node = NULL;
	_spliting_point = NULL;
	_abandon_count = 0;
	_insert_count = 0;
	_prune_count = 0;
	_prune_opposite = 0; _prune_opposite = 0;
	_cls_count = 0;
	_prune_p = 0;
	_prune_n = 0;
}

void treeManager::_reset() {
	_result_cls.clear();
	if (_p_node) {
		_destory(_p_node);
		delete _p_node;
		_p_node = NULL;
	}
	if (_n_node) {
		_destory(_n_node);
		delete _n_node;
		_n_node = NULL;
	}
	_spliting_point = NULL;
	_abandon_count = 0;
	_insert_count = 0;
	_prune_count = 0;
	_cls_count = 0;
	_prune_p = 0;
	_prune_n = 0;
	_prune_opposite = 0;
	return;
}

clsNode* treeManager::_create_node(clsNode* parent, int val) {
	clsNode* new_node = new clsNode;
	new_node->is_root = false;
	new_node->val = val;
	new_node->temp_link = NULL;
	new_node->parent_node = parent;
	new_node->is_marked = false;
	return new_node;
}

//二分查找第一个小于或等于该元素的位置
nodeLoc treeManager::_locate_branch(vector<clsNode*>& branches, int val) {
	if (branches.empty()) {
		return branches.begin();
	}
	int low, high;
	low = 0;
	high = branches.size() - 1;
	//对于所有元素都小于或大于搜索值的情况，直接返回
	if (val > branches[low]->val) {
		return branches.begin();
	}
	if (val < branches[high]->val) {
		return branches.end();
	}
	while (low < high) {
		int mid = (low + high) / 2;
		if (val > branches[mid]->val) {
			high = mid - 1;
		}
		else if (val == branches[mid]->val) {
			return branches.begin() + mid;
		}
		else {
			low = mid + 1;
		}
	}
	//如果没有找到，说明搜索结束，此时low=high，检查当前元素与val的关系即可
	if (val > branches[low]->val){
		return branches.begin() + low;
	}
	else {
		return branches.begin() + low + 1;
	}
}

clsNode* treeManager::_nevigate(clsNode* parent, int next_val) {
	vector<clsNode*>& children = parent->child_nodes;
	if (children.empty()) {
		return NULL;
	}
	int low, high;
	low = 0;
	high = children.size() - 1;
	//对于所有元素都小于或大于搜索值的情况，直接返回
	if (next_val > children[low]->val) {
		return NULL;
	}
	if (next_val < children[high]->val) {
		return NULL;
	}
	while (low <= high) {
		int mid = (low + high) / 2;
		if (next_val > children[mid]->val) {
			high = mid - 1;
		}
		else if (next_val == children[mid]->val) {
			return children[mid];
		}
		else {
			low = mid + 1;
		}
	}
	//如果没有找到，return NULL;
	return NULL;
}

void treeManager::_build_branch(const sCls& material) {
	vector<int>::const_iterator i = material.get_literals().begin();
	clsNode* cur_node = _root;
	clsNode* next_node = NULL;
	//查找前缀
	while (i != material.get_literals().end()) {
		next_node = _nevigate(cur_node, *i);
		if (next_node) {
			cur_node = next_node;
			++i;
		}
		else {
			break;
		}
	}
	//在该层查找要插入的位置
	if (i != material.get_literals().end() && !cur_node->child_nodes.empty()) {
		nodeLoc pos = _locate_branch(cur_node->child_nodes, *i);
		next_node = _create_node(cur_node,*i);
		cur_node->child_nodes.insert(pos, next_node);
		cur_node = next_node;
		++i;
	}
	while (i != material.get_literals().end()){
		next_node = _create_node(cur_node, *i);
		cur_node->child_nodes.push_back(next_node);
		cur_node = next_node;
		++i;
	}
	//加上尾巴
	cur_node->child_nodes.push_back(_create_node(cur_node, TAIL));
	return;
}

void treeManager::_build_search_tree() {
	clsLoc i = cls_manager.s_cls().begin();
	for (; i != cls_manager.s_cls().end(); ++i) {
		_build_branch(*i);
	}
	return;
}

clsNode* treeManager::_nevigate(clsNode* parent, intLoc next_val) {
	return _nevigate(parent, *next_val);
}

bool treeManager::_verify_sub_cls(clsNode* cur_node, intLoc next_point, intLoc end_point) {
	if (cur_node) {
		if (next_point == end_point) {
			if (cur_node->child_nodes[0]->val == TAIL) {
				return true;
			}
			else {
				return false;
			}
		}
		if (cur_node->child_nodes[0]->val == TAIL) {
			return false;
		}
		clsNode* next_node = _nevigate(cur_node, next_point);
		return _verify_sub_cls(next_node, ++next_point, end_point);
	}
	else {
		return false;
	}
}

nodeLoc treeManager::_loc_nevigate(clsNode* parent, int next_val) {
	vector<clsNode*>& children = parent->child_nodes;
	int low, high;
	low = 0;
	high = children.size() - 1;
	//对于所有元素都小于或大于搜索值的情况，直接返回
	if (next_val > children[low]->val) {
		return children.end();
	}
	if (next_val < children[high]->val) {
		return children.end();
	}
	while (low <= high) {
		int mid = (low + high) / 2;
		if (next_val > children[mid]->val) {
			high = mid - 1;
		}
		else if (next_val == children[mid]->val) {
			return children.begin() + mid;
		}
		else {
			low = mid + 1;
		}
	}
	//如果没有找到，return NULL;
	return children.end();
}

nodeLoc treeManager::_loc_nevigate(clsNode* parent, intLoc next_val) {
	return _loc_nevigate(parent, *next_val);
}

void treeManager::_delete_path(clsNode* start_here, intLoc next_step, intLoc end_step) {
	assert(start_here);
	if (next_step == end_step) {
		assert(start_here->child_nodes[0]->val == TAIL);
		if (start_here->child_nodes[0]->val == TAIL) {
			delete start_here->child_nodes[0];
			start_here->child_nodes.clear();
			return;
		}
		else {
			return;
		}
	}
	nodeLoc target_child = _loc_nevigate(start_here, next_step);
	assert(target_child != start_here->child_nodes.end());
	_delete_path(*target_child, next_step+1, end_step);
	if ((*target_child)->child_nodes.empty()) {
		delete *target_child;
		start_here->child_nodes.erase(target_child);
	}
	return;
}

void treeManager::_insert_clause(const sCls& s_cls) {
 	clsNode *cur_node = _root;
	clsNode *last_node = NULL;
	nodeLoc insert_pos;
	intLoc i = s_cls.get_literals().begin();
	while (i != s_cls.get_literals().end()) {
		if (cur_node->child_nodes[0]->val == TAIL) { //待插入的是长枝
			_abandon_count++;
			return;
		}
		last_node = cur_node;//更新last_node
		cur_node = _nevigate(cur_node, *i);
		if (cur_node) {
			++i;
		}
		else {
			break;
		}
	}

	if (i == s_cls.get_literals().end()) { //待插入的是短枝或相同
		_prune_count++;
		if (cur_node->child_nodes[0]->val == TAIL) {
			return;
		}
		_destory(cur_node);
		if (last_node->child_nodes.size() == 1) { //该节点为孤儿
			cur_node->child_nodes.push_back(_create_node(cur_node, TAIL));
			_delete_path(_root, s_cls.get_literals().begin(), s_cls.get_literals().end());
		}
		else { //不是孤儿节点
			nodeLoc n = _loc_nevigate(last_node, cur_node->val);
			assert(n != last_node->child_nodes.end());
			delete cur_node;
			last_node->child_nodes.erase(n);
		}
		return;
	}
	insert_pos = _locate_branch(last_node->child_nodes, *i); //不需要删除，查找可能的位置
	//检查可能消除的变量
	if ((*i) % 2 && insert_pos != last_node->child_nodes.end()){
		if ((*i) - (*(insert_pos))->val == 1) {
			_spliting_point = *(insert_pos);
		}
	}
	else if (!((*i)%2) && insert_pos != last_node->child_nodes.begin()) {
		if ((*(insert_pos - 1))->val - (*i) == 1) {
			_spliting_point = *(insert_pos-1);
		}
	}
	if (_spliting_point) {
		//验证是否需要删除
		cur_node = _spliting_point;
		if (_verify_sub_cls(cur_node, i+1, s_cls.get_literals().end())) {
			_prune_opposite++;
			//对于整条线进行删除
			_delete_path(cur_node, i+1, s_cls.get_literals().end());//cur_node以下的都做好了
			if (cur_node->child_nodes.empty()) { //没有其他枝了
				if (cur_node->parent_node->child_nodes.size() == 1) { //该节点也是孤儿，改为尾巴，进一步处理
					cur_node->val = TAIL;
					_delete_path(_root, s_cls.get_literals().begin(), i);
				}
				else {//不是孤儿，加尾巴
					cur_node->child_nodes.push_back(_create_node(cur_node, TAIL));
				}
			}
			_spliting_point = NULL;
			return;
		}
	}
	//真正的插入操作
	_insert_count++;
	cur_node = _create_node(last_node, *i);
	last_node->child_nodes.insert(insert_pos, cur_node);
	i++;
	while (i != s_cls.get_literals().end()) {
		clsNode* new_node = _create_node(cur_node, *i);
		cur_node->child_nodes.push_back(new_node);
		cur_node = new_node;
		++i;
	}
	//加上尾巴
	cur_node->child_nodes.push_back(_create_node(cur_node, TAIL));
	return;
}

//删除从root开始所有下层的树结构
void treeManager::_destory(clsNode* root) {
	vector<clsNode*>::iterator i = root->child_nodes.begin();
	for (; i != root->child_nodes.end(); ++i) {
		if ((*i)->val == TAIL) {
			delete *i;
		}
		else {
			_destory(*i);
			delete *i;
		}
	}
}

void treeManager::_count_cls(clsNode* root) {
	if (root->val == TAIL) {
		_cls_count++;
	}
	else {
		vector<clsNode*>::iterator i = root->child_nodes.begin();
		while (i != root->child_nodes.end()) {
			_count_cls(*i);
			++i;
		}
	}
}

//检查同一个句子中的重复和相反变量
bool treeManager::_check(const vector<int>& trace) {
	_material.clear();
	vector<int>::const_iterator i = trace.begin();
	_material.push_back(*(i++));
	while (i != trace.end()) {
		vector<int>::iterator j = _material.begin();
		while (j != _material.end()) { //找到_material中第一个大于*i的值
			if (*j <= *i) {
				++j;
			}
			else {
				break;
			}
		}
		if (j != _material.begin() && *i == *(j-1)) { // 检查到重复，舍弃
			++i;
		}
		else {
			if (*i % 2) { //对于奇数检查之前是否有相反变量
				if (j != _material.begin() && *(j - 1) == *i - 1) {
					return false;
				}
				else {
					_material.insert(j, *i);
					++i;
				}
			}
			else { //对于偶数检查之后是否有反变量
				if (j != _material.end() && *j == *i + 1) {
					return false;
				}
				else {
					_material.insert(j, *i);
					++i;
				}
			}
		}
	}
	return true;
}

void treeManager::_record(clsNode* cur_node) {
	if (cur_node->val == TAIL) {
		if (cur_node->temp_link == NULL) {
			//深度优先遍历终止，生成语句
			if (_check(_path)) {
				_result_cls.push_back(sCls(_material, TRANS_VAL));
			}
			return;
		}
		else {
			//p_node终止，接下来是n_node
			clsNode* next_node = cur_node->temp_link;
			vector<clsNode*>::iterator i = next_node->child_nodes.begin();
			while (i != next_node->child_nodes.end()){
				_record(*i);
				++i;
			}
			return;
		}
	}
	else {
		//一般的node
		_path.push_back(cur_node->val);
		vector<clsNode*>::iterator i = cur_node->child_nodes.begin();
		while (i != cur_node->child_nodes.end()){
			_record(*i);
			++i;
		}
		//在返回上层之前弹出本层的值
		_path.pop_back();
		return;
	}
}

//递归遍历所有的叶子
void treeManager::_multiply(clsNode* start_node) {
	if (start_node->val == TAIL) {
		start_node->temp_link = _n_node;
	}
	else {
		vector<clsNode*>::iterator i = start_node->child_nodes.begin();
		for (; i != start_node->child_nodes.end(); ++i){
			_multiply(*i);
		}
	}
	return;
}

void treeManager::_dump(vector<sCls>& container, clsNode* root){
	if (root->val == TAIL) {
		container.push_back(sCls(_path, TRANS_VAL));
		return;
	}
	else {
		if (root->is_root) {
			nodeLoc n = root->child_nodes.begin();
			while (n != root->child_nodes.end()) {
				_dump(container, *n);
				++n;
			}
		}
		else {
			_path.push_back(root->val);
			nodeLoc n = root->child_nodes.begin();
			while (n != root->child_nodes.end()) {
				_dump(container, *n);
				++n;
			}
			_path.pop_back();
		}
	}
}

void treeManager::dump() {
	cls_manager.s_cls().clear();
	_path.clear();
	_dump(cls_manager.s_cls(),_root);
}

bool treeManager::build() {
	_reset();
	int index = _root->child_nodes.back()->val;
	if (index % 2){
		//没有对应的正变量分支，直接删除即可,build不成功
		cout << "[WARNING] No matched positive branch found!" << endl;
		_destory(_root->child_nodes.back());
		delete _root->child_nodes.back();
		_root->child_nodes.pop_back();
		return false;
	}
	else {
		//确定正变量的分支
		_p_node = _root->child_nodes.back();
		_root->child_nodes.pop_back();
	}
	++index;
	if (_root->child_nodes.back()->val == index) {
		//负分支符合要求
		_n_node = _root->child_nodes.back();
		_root->child_nodes.pop_back();
	}
	else {
		//没有检测到对应的负分支，直接删除正分支,build不成功
		cout << "[WARNING] No matched negative branch found!" << endl;
		return false;
	}
	return true;
}

void treeManager::multiply() {
	_multiply(_p_node);
	return;
}
//收集所有的结果cls
void treeManager::collect() {
	vector<clsNode*>::iterator i = _p_node->child_nodes.begin();
	while (i != _p_node->child_nodes.end()) {
		_record(*i);
		++i;
	}
	cout << "[INFO] " << _result_cls.size() << " clauses generated!" << endl;
	//mp_new_cls();
	vector<sCls>::iterator j = _result_cls.begin();
	int temp = 0;
	while (j != _result_cls.end()) {
		_insert_clause(*j);
		//if (temp == 177) {
		//	cout << "heer" << endl;
		//}
		//cout << temp++ << endl;
		++j;
	}
	cout << "[INFO] Step Report: [ " << _abandon_count << " " << _insert_count << " " << _prune_count << " " << _prune_opposite << " ]" << endl;
	return;
}

void treeManager::set_up() {
	_build_search_tree();
	return;
}

int treeManager::count_clause() {
	_count_cls(_root);
	return _cls_count;
}

void treeManager::dump_new_cls() {
	vector<sCls>& temp = cls_manager.new_cls();
	for (clsLoc i = _result_cls.begin(); i != _result_cls.end(); ++i) {
		temp.push_back(*i);
	}
	return;
}
treeManager::~treeManager(){
	_destory(_root);
	delete _root;
}