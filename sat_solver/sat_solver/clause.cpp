#include "clause.h"

clause::clause():_length(0) {
}

clause::clause(const vector<int>& vars):_cls_vars(vars) {
	//_cls_vars = vars;
	_length = vars.size();
}

clause::clause(const clause& cls) {
	_cls_vars = cls.get_literals();
	_length = cls.length();
}

int clause::length() const{
	return _length;
}

const vector<int>& clause::get_literals() const{
	return _cls_vars;
}

clause ::~clause(){
}