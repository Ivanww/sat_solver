#include <iostream>
#include <fstream>
#include <string>

#define _CRT_SECURE_NO_WARNINGS 1

#include "satSolver.h"

using namespace std;

int main(int argc, char* argv[]) {
	cout << "SAT-Solver Based on Folding Startegy Verison 0.2" << endl;
	if (argc < 2) {
		cout << "[ERROR] Invalid argument number" << endl;
		cout << "[INFO] SAT-solver stopped" << endl;
		system("Pause");
		return 1;
	}
	ofstream log_file("log.txt", ios::out);
	if (!log_file) {
		cerr << "[ERROR] Can not open log file!" << endl;
		exit(1);
	}
	satSolver ss;
	if (ss.initial(argv[1])) {
		ss.do_process();
	}
	system("Pause");
	return 0;
}