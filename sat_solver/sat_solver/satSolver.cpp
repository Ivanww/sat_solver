#include "satSolver.h"

satSolver::satSolver():_cls_manager(clauseManager::get_instance()){
	_rS.round_count = 0;
	_rS.step_log = true;
	_log_os.open("log.txt", ios::out);
	if (!_log_os) {
		cout << "[WARNING] Cannot open log file! Log will be send to STDOUT" << endl;
		_rS.is_log_ok = false;
		return;
	}
	_rS.is_log_ok = true;
	return;
}

void satSolver::_proceed() {
	time_t start, stop;
	if (_tree_manager.build()) {
		_tree_manager.multiply();
		time(&start);
		_tree_manager.collect();
		time(&stop);
		cout << "[INFO] Collecting time : " << stop - start << " s" << endl;
		return;
	}
	return;
}

void satSolver::_retrieve() {
	//_tree_manager.destroy();
}

bool satSolver::initial(const string& cnf_filename) {
	_cnf_manager.filename(cnf_filename);
	if (_cnf_manager.initial(_log_os)) {
		_cls_manager.dump_s_cls(_log_os);
		return true;
	}
	else {
		cout << "[ERROR] cnfManager initializing failed! " << endl;
		return false;
	}
}

void satSolver::do_process() {
	_tree_manager.set_up();
	cout << "[INFO] " << _tree_manager.count_clause() << " clause in the database" << endl;
	while (!_cls_manager.empty_cls()) {
		_rS.round_count++;
		//if (_rS.round_count == 5) {
		//	cout << "I am Here" << endl;
		//}
		_proceed();
		cout << "[INFO] [ROUND " << _rS.round_count << "] " << _tree_manager.count_clause() << " clause in the database" << endl;
		//mp_step(_log_os);
		//for debug
		if ((_rS.round_count ==1)) {
			_tree_manager.dump();
			_cls_manager.dump_s_cls(_log_os);
			return;
		}
	}
	//�����������,SAT
	cout << "[INFO] All clauses elinimated!" << endl;
	cout << "[RESULT] SAT" << endl;
	return;
}

void satSolver::reset() {

}

void satSolver::dump_step(ostream& os) const {
	os << "====== STEP No. " << _rS.round_count << " REPORT ======" << endl;
	vector<serializeClause>::const_iterator i = _cls_manager.new_cls().begin();
	int temp_cls_count = 0;
	for (; i != _cls_manager.new_cls().end(); ++i) {
		temp_cls_count++;
		os << "[CLAUSE " << temp_cls_count << "]\t";
		vector<int>::const_iterator j = i->get_literals().begin();
		for (; j != i->get_literals().end(); ++j) {
			os << *j << "\t";
		}
		os << endl;
	}
	os << "[INFO] ====== \tDump step data finished\t ======" << endl;
	return;
}

satSolver ::~satSolver() {
	if (_rS.is_log_ok) {
		_log_os.close();
	}
}