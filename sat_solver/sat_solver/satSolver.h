#ifndef __SAT_SOLVER__
#define __SAT_SOLVER__
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>

#include "clauseManager.h"
#include "cnfManager.h"
#include "treeManager.h"

using namespace std;
struct runningStatus {
	bool is_log_ok;
	bool step_log;
	int round_count;
};
class satSolver {
private:

	ofstream _log_os;
	cnfManager _cnf_manager;
	clauseManager& _cls_manager;
	treeManager _tree_manager;
	runningStatus _rS;

	void _proceed();
	void _retrieve();
public:
	
	satSolver ();

	void log(const string&);
	//void log(ostream&);

	bool initial(const string&);
	void do_process();
	void reset();
	void dump_step(ostream&) const;
	~satSolver ();
};

#endif