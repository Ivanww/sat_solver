#include "serializeClause.h"

serializeClause::serializeClause() : clause() {
}

serializeClause::serializeClause(const vector<int>& vars, valType val_type): clause(vars) {
	if (val_type == ORI_VAL) {
		_trans_vars();
	}
	//对于变量进行排序,冒泡
	_sort_vars();
	_first_var = _cls_vars.front();
}

//从cls到s_cls的构造函数
serializeClause::serializeClause(const clause& cls): clause(cls) {
	_trans_vars();
	_sort_vars();
	_first_var = _cls_vars.front();
}

//复制构造函数
serializeClause::serializeClause(const serializeClause& s_cls): clause() {
	_cls_vars = s_cls.get_literals(); 
	_length = s_cls.length();
	_first_var = s_cls.first_var();
}

//比较两个sCls,相同返回真
bool sCls::compare(const sCls& that) const{
	if (that.length() == _length) {
		vector<int>::const_iterator i = that.get_literals().begin();
		vector<int>::const_iterator j = _cls_vars.begin();
		for (; j != _cls_vars.end(); ++i, ++j) {
			if (*i != *j) {
				return false;
			}
		}
	}
	else {
		return false;
	}
	return true;
}

//把变量排序
void serializeClause::_sort_vars() {
	bool has_sorted = true;
	vector<int>::iterator head = _cls_vars.begin();
	vector<int>::iterator tail = _cls_vars.end();
	for (; tail != head + 1; --tail) {
		for (vector<int>::iterator i = head; i != tail - 1; ++i){
			if (*i > *(i + 1)) {
				has_sorted = false;
				int temp = *i;
				*i = *(i + 1);
				*(i + 1) = temp;
			}
		}
		if (has_sorted) {
			break;
		}
	}
}
//转换变量:1->2,-1->2+1 
void serializeClause::_trans_vars() {
	vector<int>::iterator i = _cls_vars.begin();
	for (; i != _cls_vars.end(); ++i) {
		if (*i > 0) {
			*i = *i * 2;
		}
		else {
			*i = (-*i) * 2 + 1;
		}
	}
}

int serializeClause::first_var() const{
	return _first_var;
}

bool serializeClause::operator< (const serializeClause& r) {
	if (this->_first_var < r.first_var()) {
		return true;
	}
	else {
		return false;
	}
}

bool serializeClause::operator>(const serializeClause& r) {
	if (this->_first_var > r.first_var()) {
		return true;
	}
	else {
		return false;
	}
}

bool serializeClause::operator==(const serializeClause& r) {
	if (this->_first_var == r.first_var()) {
		return true;
	}
	else {
		return false;
	}
}

serializeClause::~serializeClause() {

}