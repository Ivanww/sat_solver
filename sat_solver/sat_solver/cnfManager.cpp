#include "cnfManager.h"


cnfManager::cnfManager():cls_manager(clauseManager::get_instance()){
	_stat_info.file_name = "";
	_stat_info.clause_num = 0;
	_stat_info.var_num = 0;
	_stat_info.is_cls_checked = false;
	_stat_info.is_var_checked = true;
	_var_count = NULL;
}
cnfManager::cnfManager(const string& filename) :cls_manager(clauseManager::get_instance()){
	_stat_info.file_name = filename;
	_stat_info.clause_num = 0;
	_stat_info.var_num = 0;
	_stat_info.is_cls_checked = false;
	_stat_info.is_var_checked = true;
	_var_count = NULL;
}

int cnfManager::_abs(int v) {
	return v >= 0 ? v : -v;
}

void cnfManager::_read_cnf(const string& filename) {
	char lineBuffer[MAX_LINE_LENGTH];
	char wordBuffer[MAX_WORD_LENGTH];
	vector<int> vars;
	int line_count = 0;
	int clause_num = 0;
	int var_num = 0;

	ifstream cnfFile(filename, ios::in);

	if (!cnfFile) {
		cerr << "[ERROR] Can not open file "<< filename << endl;
		return;
	}

	while (cnfFile.getline(lineBuffer, MAX_LINE_LENGTH)) {
		line_count++;
		if (!(line_count % 100000)) {
			cout << "[INFO] " << line_count << " lines are recorded!" << endl;
		}
		if (lineBuffer[0] == 'c') {
			cout << "[INFO] Comment line captured!" << endl;
			continue;
		}
		else if (lineBuffer[0] == 'p') {
			cout << "[INFO] P line captured!" << endl;
			int arg = sscanf(lineBuffer, "p cnf %d %d", &(_stat_info.var_num), &(_stat_info.clause_num));
			if (arg < 2) {
				cerr << "[ERROR] Format error in " << filename << " line "<< line_count << endl;
				return;
			}
			_var_count = new int[_stat_info.var_num+1];
			fill(&_var_count[0], &_var_count[_stat_info.var_num+1], 0);
		}
		else {
			char* pLine = lineBuffer;
			while (*pLine) {
				char* pWord = wordBuffer;
				//过滤掉空白
				while (*pLine && (*pLine == ' ' || *pLine == '\t')) {
					++pLine;
				}
				//空白之间的内容
				while (*pLine && (*pLine != ' ') && (*pLine != '\t') && (*pLine != '\n')) {
					*(pWord++) = *(pLine++);
				}
				//使word成为一个字符串
				*pWord = '\0';

				if (strlen(wordBuffer)) {
					//如果有数字被捕获
					int var = atoi(wordBuffer);
					_var_count[_abs(var)]++;
					if (var) {
						vars.push_back(var);
					}
					else {
						clause temp_clause(vars);
						cls_manager.add_cls(temp_clause);
						vars.clear();
					}
				}
			}
		}
	}
	cnfFile.close();
	//读取数据完毕，对于sCls进行整理
	cout << "[INFO] " << line_count << " lines in total!" << endl;
	cls_manager.sort_s_cls();
	//检查情况
	cout << "[INFO] Checking variables and clauses..." << endl;
	for (int i = 1; i <= _stat_info.var_num; i++) {
		if (_var_count[i] <= 0) {
			_stat_info.is_var_checked = false;
			break;
		}
	}
	if (cls_manager.cls_num() == _stat_info.clause_num) {
		_stat_info.is_cls_checked = true;
	}
}

void cnfManager::dump_vars(ostream& os) const{
	os << "Var\tCount\tVar\tCount\tVar\tCount" << endl;
	for (int i = 1; i < _stat_info.var_num + 1; i++) {
		os << i << " :\t" << _var_count[i] << "\t";
		if (!(i % 3)) {
			os << endl;
		}
	}
	os << endl;
	return;
}
void cnfManager::filename(const string& cnf_filename) {
	_stat_info.file_name = cnf_filename;
}


void cnfManager::_report_cnf(ostream& os) const{
	os << "\t\t====== CNF file Report ======" << endl;
	os << "[INFO] Total Variable : " << _stat_info.var_num << endl;
	os << "[INFO] Total Clause : " << _stat_info.clause_num << endl;
	if (_stat_info.is_var_checked) {
		os << "[SUCCESS] Variables number checking successful" << endl;
	}
	else {
		os << "[WARNING] Variables number checking unsuccessful" << endl;
		os << "[WARNING] Some variables may missing" << endl;
	}
	if (_stat_info.is_cls_checked) {
		os << "[SUCCESS] Clause number checking successful" << endl;
	}
	else {
		os << "[WARNING] Clause number checking unsuccessful" << endl;
		os << "[WARNING] Some clauses may missing" << endl;
	}
	dump_vars(os);
	os << "\t\t====== CNF file Report End ======" << endl;
}

bool cnfManager::initial() {
	if (_stat_info.file_name == "") {
		cout << "[ERROR] File name missing! " << endl;
		return false;
	}
	else {
		_read_cnf(_stat_info.file_name);
		_report_cnf(cout);
		return true;
	}
}

bool cnfManager::initial(ostream& log) {
	if (_stat_info.file_name == "") {
		cout << "[ERROR] File name missing! " << endl;
		return false;
	}
	else {
		_read_cnf(_stat_info.file_name);
		_report_cnf(log);
		return true;
	}
}


cnfManager::~cnfManager(){
}