#ifndef __CNF_MANAGER__
#define __CNF_MANAGER__

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>

#include "clauseManager.h"

using namespace std;

const int MAX_LINE_LENGTH = 65536;
const int MAX_WORD_LENGTH = 64;

struct cnfFileStat {
	string file_name;
	int clause_num;
	int var_num;
	bool is_cls_checked;
	bool is_var_checked;
};


class cnfManager{
private:
	
	int* _var_count;
	cnfFileStat _stat_info;
	clauseManager& cls_manager;
	
	int _abs(int);
	void _read_cnf(const string&);
	void _report_cnf(ostream&) const;

public:
	
	cnfManager();
	cnfManager(const string&);

	int remain_cls();
	void remain_cls(int);
	
	void filename(const string&);
	/*初始化：读文件，生成关于文件检查的报告*/
	bool initial();
	bool initial(ostream&);
	/*返回cnf文件名*/
	const string& get_cnfname() const;

	void dump_vars(ostream&) const;
	/*生成现状的报告*/
	void dump_step(ostream&) const;

	~cnfManager();
};


#endif // !__CNF_MANAGER__


