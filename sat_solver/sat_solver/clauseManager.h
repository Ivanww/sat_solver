#ifndef __CLAUSE_MANAGER__
#define __CLAUSE_MANAGER__
#include <iostream>
#include <vector>
#include <time.h>

#include "clause.h"
#include "serializeClause.h"

using namespace std;

typedef vector<sCls>::iterator clsLoc;

class clauseManager {
private:
	vector<Cls> _cls_database;
	vector<sCls> _s_cls_database;

	vector<sCls> _s_new_cls;

	clauseManager();
	/*插入单条sCls*/
	void _insert_clause(sCls&);

	/*快速排序相关函数*/
	void _swap_s_cls(clsLoc, clsLoc);
	clsLoc _partition(clsLoc, clsLoc);
	void _quick_sort(clsLoc, clsLoc);
	
	/*二分查找插入点*/
	clsLoc _bin_search(const sCls&);
	
	/*检查重复情况*/
	bool _find_same(const sCls&, clsLoc);

	void _bubble_sort();
public:

	static clauseManager& get_instance();

	vector<sCls>& new_cls();
	/*插入单条Cls,读文件时用*/
	void add_cls(const Cls&);
	/*插入多条sCls*/
	void add_s_cls(vector<sCls>&);
	/*对sCls进行排序*/
	void sort_s_cls();
	/*获取原始语句数量*/
	int cls_num();
	/*获取剩余语句数量*/
	int remain_cls_num();
	/*返回最顶层的语句*/
	sCls& top_s_cls();
	/*弹出最顶层的语句*/
	void pop_s_cls();
	/*dump所有的语句*/
	void dump_cls(ostream&) const;
	/*dump所有还需要处理的语句*/
	void dump_s_cls(ostream&) const;
	void dump_step(ostream&) const;
	/*检查是否还有剩余的cls*/
	bool empty_cls();
	/*检查当前是否有冲突存在*/
	bool check_conflict();
	vector<sCls>& s_cls();
	~clauseManager();
};

#endif // !__CLAUSE_MANAGER__
