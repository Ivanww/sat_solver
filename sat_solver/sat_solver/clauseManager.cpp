#include "clauseManager.h"


clauseManager::clauseManager(){
}

//获取牌库的引用
clauseManager& clauseManager::get_instance() {
	static clauseManager instance;
	return instance;
}

vector<sCls>& clauseManager::new_cls() {
	_s_new_cls.clear();
	return _s_new_cls;
}

//快排的划分函数
clsLoc clauseManager::_partition(clsLoc lo, clsLoc hi) {
	sCls key = *lo;
	clsLoc i = lo, j = hi;
	while (true) {
		while (key < *i || key == *i) {
			if (i == hi) {
				break;
			}
			++i;
		}
		while (key > *j || key == *j) {
			if (j == lo) {
				break;
			}
			--j;
		}
		if (i >= j) {
			break;
		}
		_swap_s_cls(i, j);
	}
	_swap_s_cls(lo, j);
	return j;

}
//快排的交换函数
void clauseManager::_swap_s_cls(clsLoc a, clsLoc b) {
	sCls temp_s_cls(*a);
	*a = *b;
	*b = temp_s_cls;
}
//快排内部函数
void clauseManager::_quick_sort(clsLoc lo, clsLoc hi) {
	clsLoc j = _partition(lo, hi);
	if (j > lo) {
		_quick_sort(lo, j - 1);
	}
	if (j < hi) {
		_quick_sort(j + 1, hi);
	}
	return;
}

//二分查找段的位置
clsLoc clauseManager::_bin_search(const sCls& s_cls) {
	int low, high, mid;
	low = 0;
	high = _s_cls_database.size() - 1;
	while (low < high) {
		mid = (low + high) / 2;
		if (_s_cls_database[mid] < s_cls || _s_cls_database[mid] == s_cls) {
			high = mid - 1;
		}
		else if (_s_cls_database[mid] > s_cls) {
			low = mid + 1;
		}
	}
	//经过循环以后，low == high，在相同首元素的第一位
	return _s_cls_database.begin() + low;
}

//冒泡排序
void clauseManager::_bubble_sort() {
	clsLoc head = _s_cls_database.begin(), tail = _s_cls_database.end() - 1;
	bool need_sort = false;
	//最后的元素为新加的元素，所以从后往前遍历。如果遇到相等的不交换
	for (clsLoc i = head; i != tail; ++i) {
		need_sort = false;
		for (clsLoc j = tail; j != i; --j) {
			if (*j > *(j - 1)) {
				need_sort = true;
				_swap_s_cls(j, j - 1);
			}
		}
		if (!need_sort) {
			break;
		}
	}
	return;
}

//查找是否有相同的语句，如果找到相同的返回真
bool clauseManager::_find_same(const sCls& s_cls, clsLoc start_point) {
	bool has_same = false;
	int segment = s_cls.first_var();
	clsLoc i = start_point;
	//先检查是否存在同名段
	if ((*i).first_var() == segment) {
		while (i>_s_cls_database.begin() && (*i).first_var() == segment) {
			if (!s_cls.compare(*i)) {
				return false;
			}
			++i;
		}
	}
	else {
		return false;
	}
	return true;
}

//插入一个serial的clause
void clauseManager::_insert_clause(sCls& s_cls) {
	if (_s_cls_database.empty()) {
		_s_cls_database.push_back(s_cls);
		return;
	}
	clsLoc segment_loc = _bin_search(s_cls);
	if (!_find_same(s_cls, segment_loc)) {
		if ((*segment_loc).first_var() == s_cls.first_var()) {
			_s_cls_database.insert(segment_loc, s_cls);
		}
		else {
			_s_cls_database.insert(segment_loc + 1, s_cls);
		}
	}
	
	return;
}

//增加一个clause,自动创建seriable的副本
void clauseManager::add_cls(const Cls& cls) {
	_cls_database.push_back(cls);
	//
	sCls temp(cls);
	_s_cls_database.push_back(temp);
	//_insert_clause(temp);
	return;
}

//对于所有的s_cls进行排序，使用快速排序的方法
void clauseManager::sort_s_cls() {
	clsLoc lo = _s_cls_database.begin(), hi = _s_cls_database.end() - 1;
	_quick_sort(lo, hi);
}

void clauseManager::add_s_cls(vector<sCls>& result) {
	vector<sCls>::iterator i = result.begin();
	for (; i != result.end(); ++i){
		_insert_clause(*i);
	}
	return;
}
//获取实际clause的数目
int clauseManager::cls_num() {
	return _cls_database.size();
}

int clauseManager::remain_cls_num() {
	return _s_cls_database.size();
}

sCls& clauseManager::top_s_cls() {
	return _s_cls_database.back();
}

void clauseManager::pop_s_cls() {
	_s_cls_database.pop_back();
	return;
}

bool clauseManager::empty_cls() {
	return _s_cls_database.empty();
}

bool clauseManager::check_conflict() {
	int hit_val = 0;
	vector<sCls>::iterator i = _s_cls_database.begin();
	for (; i != _s_cls_database.end(); ++i) {
		if ((*i).length() > 1) {
			//如果一个cls变量大于1，还不到被关注的时候
			continue;
		}
		else {
			if ((*i).first_var() % 2) {
				if ((*i).first_var() - hit_val == 1) {
					//相反变量同时命中
					return true;
				}
				else {
					//单个反变量命中
					continue;
				}
			}
			else {
				//单个正变量命中
				hit_val = (*i).first_var();
			}
		}
	}
	return false;
}

//输出信息
void clauseManager::dump_cls(ostream& os) const{
	os << "[INFO] ====== \tDump clause data \t ======" << endl;
	vector<clause>::const_iterator i = _cls_database.begin();
	int temp_cls_count = 0;
	for (; i != _cls_database.end(); ++i) {
		temp_cls_count++;
		os << "[CLAUSE " << temp_cls_count << "]\t";
		vector<int>::const_iterator j = i->get_literals().begin();
		for (; j != i->get_literals().end(); ++j) {
			os << *j << "\t";
		}
		os << endl;
	}
	os << "[INFO] ====== \tDump clause data finished\t ======" << endl;
}


void clauseManager::dump_s_cls(ostream& os) const{
	os << "[INFO] ====== \tDump Serialize Clause Data \t ======" << endl;
	vector<serializeClause>::const_iterator i = _s_cls_database.begin();
	int temp_cls_count = 0;
	for (; i != _s_cls_database.end(); ++i) {
		temp_cls_count++;
		os << "[CLAUSE " << temp_cls_count << "]\t";
		vector<int>::const_iterator j = i->get_literals().begin();
		for (; j != i->get_literals().end(); ++j) {
			os << *j << "\t";
		}
		os << endl;
	}
	os << "[INFO] ====== \tDump serialize clause data finished\t ======" << endl;
}

vector<sCls>& clauseManager::s_cls() {
	return _s_cls_database;
}

clauseManager::~clauseManager(){
}