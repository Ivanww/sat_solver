#ifndef __TREE__
#define __TREE__

#include <iostream>
#include <vector>

using namespace std;

struct node{
	int val;
	//int child_num;
	bool is_root;
	node* left_child;
	node* right_child;
};

class bintree {
private:
	node* _root;

	void _build(const vector<int>&);

public:
	bintree ();
	const node& search_val(int);
	void add_node(int);
	void delete_node(int);
	void delete_node(node*);

	~bintree ();

};
#endif