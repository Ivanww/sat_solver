#ifndef __MULTI_SEG_BIN_CLAUSE__
#define __MULTI_SEG_BIN_CLAUSE__

#define SEGMENT_NUM 4
#include <iostream>
#include <vector>
#include <bitset>
#include "BinClause.h"

using namespace std;

class MultiSegBinClause : public BinClause{
private:
	bitset<MAX_LITERAL_NUM / SEGMENT_NUM> _seg_literals[SEGMENT_NUM];
	bitset<MAX_LITERAL_NUM / SEGMENT_NUM> _seg_signs[SEGMENT_NUM];

	void _dispatch();
	void _reset(int);
	IMP_TYPE _is_implicate(const MultiSegBinClause&, int);
public:
	MultiSegBinClause();
	MultiSegBinClause(const Cls&);
	MultiSegBinClause(const vector<int>&);
	MultiSegBinClause(int*, int);
	MultiSegBinClause(const bitset<MAX_LITERAL_NUM>&, const bitset<MAX_LITERAL_NUM>&);
	MultiSegBinClause(const BinClause&);

	void split();
	bitset<MAX_LITERAL_NUM / SEGMENT_NUM> seg_literals(int seg) const { return _seg_literals[seg]; }
	bitset<MAX_LITERAL_NUM / SEGMENT_NUM> seg_signs(int seg) const { return _seg_signs[seg]; }
	IMP_TYPE relation(const MultiSegBinClause&, int);
	MultiSegBinClause operator*(const MultiSegBinClause&);
};

typedef MultiSegBinClause MSBinCls;

#endif