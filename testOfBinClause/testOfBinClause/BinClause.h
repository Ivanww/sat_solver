#ifndef __BIN_CLAUSE__
#define __BIN_CLAUSE__

#include <iostream>
#include <vector>
#include <bitset>
#include <assert.h>

#include "Clause.h"
#define MAX_LITERAL_NUM 128

using namespace std;

enum IMP_TYPE {
	IMPLICATE = 1,
	SAME_LITERALS,
	SAME_CLAUSE,
	IMPLICATED,
	NOT_IMPLICATE,
	UNKNOWN
};


class BinClause : public Cls{
private:
	bitset<MAX_LITERAL_NUM> _bin_literals;
	bitset<MAX_LITERAL_NUM> _bin_signs;
protected:
	int _group_id;
	bool _group_sign;

	int _abs(int i) { return i > 0 ? i : -i; }
	void _translate();

public:
	BinClause();
	BinClause(const Cls&);
	BinClause(const vector<int>&);
	BinClause(int*, int);
	BinClause(const bitset<MAX_LITERAL_NUM>&, const bitset<MAX_LITERAL_NUM>&);
	//BinClause(const BinClause&);

	void erase(int);
	void split();
	int groupid() const{ return _group_id; }
	bool groupsign() const { return _group_sign; }
	const bitset<MAX_LITERAL_NUM>& bin_literals() const { return _bin_literals; }
	void update(const bitset<MAX_LITERAL_NUM>&, const bitset<MAX_LITERAL_NUM>&);
	const bitset<MAX_LITERAL_NUM>& bin_signs() const{ return _bin_signs; }
	void show(ostream&) const;

	BinClause operator*(const BinClause&);
	~BinClause();
};


#endif