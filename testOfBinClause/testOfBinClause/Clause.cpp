#include "clause.h"

clause::clause(){
}

clause::clause(const vector<int>& vars) : _literals(vars) {
	_length = vars.size();
}

clause::clause(const clause& cls) {
	_literals = cls.readable_literals();
	_length = cls.length();
}

int clause::length() const{
	return _length;
}

const vector<int>& clause::readable_literals() const{
	return _literals;
}

clause ::~clause(){
}