#include <iostream>
#include <bitset>
#include "BinClause.h"
#include "MultiSegClause.h"

using namespace std;

int main() {

	int int_array1[10] = { 11, -27 };
	int int_array2[13] = { 11, -27, -36, 77 };

	MSBinCls clause_1(int_array1, 2);
	MSBinCls clause_2(int_array2, 4);

	/*bitset<MAX_LITERAL_NUM> test_result = ~(~clause_1.bin_signs() & ~clause_1.bin_literals() | clause_1.bin_signs() & clause_2.bin_signs() \
		| ~clause_1.bin_signs() & ~clause_2.bin_signs() & clause_2.bin_literals());*/
	IMP_TYPE t = clause_2.relation(clause_1, 1);
	if (t != IMPLICATE) {
		cout << "No" << endl;
	}
	else {
		cout << "Yes" << endl;
		bitset<MAX_LITERAL_NUM> sign = clause_1.bin_signs() & clause_2.bin_signs();
		bitset<MAX_LITERAL_NUM> lit = ~clause_1.bin_signs() & clause_1.bin_literals() & ~clause_2.bin_signs() | sign;
		MSBinCls result(lit, sign);
		result.show(cout);
	}
	system("Pause");
	return 0;
}