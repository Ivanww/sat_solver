#include "clause.h"

clause::clause(){
}

clause::clause(const vector<int>& vars) : _literals(vars) {
}

clause::clause(const clause& cls) {
	_literals = cls.literals();
	_length = cls.length();
}

int clause::length() const{
	return _literals.size();
}

const vector<int>& clause::literals() const{
	return _literals;
}

clause ::~clause(){
}