#include "satSolver.h"

satSolver::satSolver(const string& file_name) :_cls_manager(ClauseManager::get_instance()), _file_manager(file_name), _step_num(0){
	_log_os.open("log.txt", ios::out);
	if (!_log_os) {
		cout << "[WARNING] Cannot open log file! Log will be send to STDOUT" << endl;

		return;
	}
	_is_log_ok = true;
	_rs.round_count = 0;
	return;
}

void satSolver::_proceed() {
	time_t start, stop;
	if (_tree_manager.build()) {
		_tree_manager.multiply();
		time(&start);
		_tree_manager.collect();
		time(&stop);
		cout << "[INFO] Collecting time : " << stop - start << " s" << endl;
		_tree_manager.retrieve();
		return;
	}
	return;
}

bool satSolver::initial() {
	if (_file_manager.initial(cout)) {
		_file_manager.dump_s_cls(_log_os);
		return true;
	}
	else {
		cout << "[ERROR] cnfManager initializing failed! " << endl;
		return false;
	}
}

void satSolver::solve() {
	_tree_manager.init_tree();
	cout << "[INFO] " << _tree_manager.count_clause() << " clause in the database" << endl;
	while (!_cls_manager.empty_cls()) {
		_rs.round_count++;
		_proceed();
		cout << "[INFO] [ROUND " << _rs.round_count << "] " << _tree_manager.count_clause() << " clause in the database" << endl;
		cout << "[INFO] GENERATE_CLS_TIME : " << _tree_manager.gen_cls_time() << " ms" << endl;
		cout << "[INFO] CHECKING_TIME : " << _tree_manager.check_time() << " ms" << endl;
		if (_rs.round_count == 40) {
			_file_manager.dump_s_cls(_log_os);
			return;
		}
	}
	//�����������,SAT
	cout << "[INFO] All clauses elinimated!" << endl;
	cout << "[RESULT] SAT" << endl;
	return;
}

satSolver ::~satSolver() {
	if (_is_log_ok) {
		_log_os.close();
	}
}