#ifndef __SAT_SOLVER__
#define __SAT_SOLVER__
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>

#include "ClauseManager.h"
#include "FileManager.h"
#include "TreeManager.h"

using namespace std;
typedef struct RunningStatus {
	int round_count;
} Rs;
class satSolver {
private:
	ofstream _log_os;
	FileManager _file_manager;
	ClauseManager& _cls_manager;
	TreeManager _tree_manager;
	Rs _rs;

	int _step_num;
	bool _is_log_ok;
	void _proceed();
public:

	satSolver(const string&);
	bool initial();
	void solve();
	~satSolver();
};

#endif