#ifndef __TREE__MANAGER__
#define __TREE__MANAGER__
#include <iostream>
#include <vector>
#include <ctime>
#include <assert.h>

#include "ClauseManager.h"

#define TAIL 1

using namespace std;

struct clsNode {
	int val;
	bool is_root;
	bool is_marked;
	clsNode* temp_link;
	clsNode* parent_node;
	vector<clsNode*> child_nodes;
};



typedef vector<clsNode*>::iterator nodeLoc;
typedef vector<int>::const_iterator intLoc;

class TreeManager{
private:
	ClauseManager& cls_manager;
	vector<serializeClause> _result_cls;
	vector<int> _path;
	vector<int> _material;
	clsNode* _root;
	clsNode *_p_node, *_n_node;
	int _cls_count;

	time_t _start, _stop;
	time_t _g_cls_time, _check_time;

	void _insert_path(clsNode*, intLoc, intLoc);
	void _insert_path(sCls&, clsNode*);
	clsNode* _create_node(clsNode*, int);
	clsNode* _nevigate(clsNode*, int);
	nodeLoc _loc_nevigate(clsNode*, int);

	void _reset();
	void _record(clsNode*);
	bool _check(const vector<int>&);
	void _multiply(clsNode*);
	void _count_cls(clsNode*);
	void _destory(clsNode*);
	void _retrieve(vector<sCls>&, clsNode*);
	//优化尝试
	//插入和剪枝
	void _insert_clause(const sCls&);
	nodeLoc _locate_branch(vector<clsNode*>&, int);
	clsNode* _prefix(sCls&);
	//提前消除
	clsNode* _nevigate(clsNode*, intLoc);
	nodeLoc _loc_nevigate(clsNode*, intLoc);
	void _delete_path(clsNode*, intLoc, intLoc);
public:

	TreeManager();
	void init_tree();
	bool build();
	void multiply();
	void collect();
	void destroy();
	void retrieve();
	int count_clause();
	time_t check_time() { return _check_time; }
	time_t gen_cls_time() { return _g_cls_time; }
	~TreeManager();
};

#endif