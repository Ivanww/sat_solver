#include "ClauseManager.h"


ClauseManager::ClauseManager(){
}

//获取牌库的引用
ClauseManager& ClauseManager::get_instance() {
	static ClauseManager instance;
	return instance;
}

//增加一个clause,自动创建seriable的副本
void ClauseManager::add_cls(const Cls& cls) {
	_cls_database.push_back(cls);
	_s_cls_database.push_back(sCls(cls));
	return;
}

void ClauseManager::update_s_cls(vector<sCls>& result) {
	_s_cls_database.clear();
	vector<sCls>::iterator i = result.begin();
	for (; i != result.end(); ++i){
		_s_cls_database.push_back(*i);
	}
	return;
}

bool ClauseManager::empty_cls() {
	return _s_cls_database.empty();
}

vector<sCls>& ClauseManager::s_cls() {
	return _s_cls_database;
}

vector<Cls>& ClauseManager::cls() {
	return _cls_database;
}

ClauseManager::~ClauseManager(){
}