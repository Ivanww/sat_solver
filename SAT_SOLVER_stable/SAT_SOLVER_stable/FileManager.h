#ifndef __CNF_MANAGER__
#define __CNF_MANAGER__

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>

#include "ClauseManager.h"

using namespace std;

const int MAX_LINE_LENGTH = 65536;
const int MAX_WORD_LENGTH = 64;

struct CnfFileStat {
	string file_name;
	int clause_num;
	int var_num;
	bool is_cls_checked;
	bool is_var_checked;
};


class FileManager{
private:

	int* _var_count;
	CnfFileStat _stat_info;
	ClauseManager& _r_cls_manager;

	int _abs(int);
	void _read_cnf(const string&);
	void _report_cnf(ostream&) const;

	void _check_literals();
	void _check_clauses();

public:

	FileManager(const string&);

	/*初始化：读文件，生成关于文件检查的报告*/
	bool initial(ostream&);

	void dump_vars(ostream&) const;
	/*生成现状的报告*/
	void dump_step(ostream&) const;
	/*dump所有的语句*/
	void dump_cls(ostream&) const;
	/*dump所有还需要处理的语句*/
	void dump_s_cls(ostream&) const;
	~FileManager();
};


#endif // !__CNF_MANAGER__


