#include "serializeClause.h"

serializeClause::serializeClause() : clause() {
}

serializeClause::serializeClause(const vector<int>& vars, literalType val_type) : clause(vars) {
	if (val_type == ORI_VAL) {
		_trans_vars();
	}
	//对于变量进行排序,冒泡
	_sort_vars();
}

//从cls到s_cls的构造函数
serializeClause::serializeClause(const clause& cls) : clause(cls) {
	_trans_vars();
	_sort_vars();
}

//复制构造函数
serializeClause::serializeClause(const serializeClause& s_cls) : clause() {
	_literals = s_cls.literals();
}

//把变量排序
void serializeClause::_sort_vars() {
	bool has_sorted = true;
	vector<int>::iterator head = _literals.begin();
	vector<int>::iterator tail = _literals.end();
	for (; tail != head + 1; --tail) {
		for (vector<int>::iterator i = head; i != tail - 1; ++i){
			if (*i > *(i + 1)) {
				has_sorted = false;
				int temp = *i;
				*i = *(i + 1);
				*(i + 1) = temp;
			}
		}
		if (has_sorted) {
			break;
		}
	}
}
//转换变量:1->2,-1->2+1 
void serializeClause::_trans_vars() {
	vector<int>::iterator i = _literals.begin();
	for (; i != _literals.end(); ++i) {
		if (*i > 0) {
			*i = *i * 2;
		}
		else {
			*i = (-*i) * 2 + 1;
		}
	}
}

serializeClause::~serializeClause() {

}