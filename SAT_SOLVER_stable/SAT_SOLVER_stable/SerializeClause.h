#ifndef __SERIALIZE_CLAUSE__
#define __SERIALIZE_CLAUSE__

#include <iostream>
#include <vector>

#include "clause.h"

using namespace std;

enum literalType {
	ORI_VAL = 1,
	TRANS_VAL
};

class serializeClause : public clause{
private:

	void _trans_vars();
	void _sort_vars();

public:

	serializeClause();
	serializeClause(const vector<int>&, literalType);
	serializeClause(const clause&);
	serializeClause(const serializeClause&);

	~serializeClause();
};

typedef serializeClause sCls;

#endif