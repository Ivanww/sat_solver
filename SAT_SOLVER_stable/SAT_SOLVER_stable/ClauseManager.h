#ifndef __CLAUSE_MANAGER__
#define __CLAUSE_MANAGER__
#include <iostream>
#include <vector>
#include <time.h>

#include "Clause.h"
#include "SerializeClause.h"

using namespace std;

typedef vector<sCls>::iterator sClsLoc;

class ClauseManager {
private:
	vector<Cls> _cls_database;
	vector<sCls> _s_cls_database;

	ClauseManager();
public:
	static ClauseManager& get_instance();

	/*插入单条Cls,读文件时用*/
	void add_cls(const Cls&);
	/*覆盖sCls*/
	void update_s_cls(vector<sCls>&);
	/*检查是否还有剩余的cls*/
	bool empty_cls();
	int cls_num() { return _s_cls_database.size(); }
	vector<sCls>& s_cls();
	vector<Cls>& cls();
	~ClauseManager();
};

#endif // !__CLAUSE_MANAGER__
