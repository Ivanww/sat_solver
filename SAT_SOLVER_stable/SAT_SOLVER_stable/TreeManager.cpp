#include "TreeManager.h"

TreeManager::TreeManager() :
cls_manager(ClauseManager::get_instance()),
_root(new clsNode),
_p_node(NULL),
_n_node(NULL)
{
	_root = new clsNode;
	_root->is_root = true;
	_root->val = 0;
	_root->temp_link = NULL;
	_root->is_marked = false;
	_root->parent_node = NULL;
	_cls_count = 0;
}

void TreeManager::_reset() {
	_result_cls.clear();
	if (_p_node) {
		_destory(_p_node);
		delete _p_node;
		_p_node = NULL;
	}
	if (_n_node) {
		_destory(_n_node);
		delete _n_node;
		_n_node = NULL;
	}
	_cls_count = 0;
	_g_cls_time = 0;
	_check_time = 0;
	return;
}

clsNode* TreeManager::_create_node(clsNode* parent, int val) {
	clsNode* new_node = new clsNode;
	new_node->is_root = false;
	new_node->val = val;
	new_node->temp_link = NULL;
	new_node->parent_node = parent;
	new_node->is_marked = false;
	return new_node;
}
/*二分查找第一个小于或等于该元素的位置*/
nodeLoc TreeManager::_locate_branch(vector<clsNode*>& branches, int val) {
	if (branches.empty()) {
		return branches.begin();
	}
	int low, high;
	low = 0;
	high = branches.size() - 1;
	//对于所有元素都小于或大于搜索值的情况，直接返回
	if (val > branches[low]->val) {
		return branches.begin();
	}
	if (val < branches[high]->val) {
		return branches.end();
	}
	while (low <= high) {
		int mid = (low + high) / 2;
		if (val > branches[mid]->val) {
			high = mid - 1;
		}
		else if (val == branches[mid]->val) {
			return branches.begin() + mid;
		}
		else {
			low = mid + 1;
		}
	}
	//如果没有找到，说明搜索结束，此时low=high，检查当前元素与val的关系即可
	if (val > branches[low]->val){
		return branches.begin() + low;
	}
	else {
		return branches.begin() + low + 1;
	}
}
/*找到指定的子节点，返回该节点的迭代器，否则返回end()*/
nodeLoc TreeManager::_loc_nevigate(clsNode* parent, int next_val) {
	if (parent->child_nodes.empty()) {
		return parent->child_nodes.end();
	}
	vector<clsNode*>& children = parent->child_nodes;
	int low, high;
	low = 0;
	high = children.size() - 1;
	//对于所有元素都小于或大于搜索值的情况，直接返回
	if (next_val > children[low]->val) {
		return children.end();
	}
	if (next_val < children[high]->val) {
		return children.end();
	}
	while (low <= high) {
		int mid = (low + high) / 2;
		if (next_val > children[mid]->val) {
			high = mid - 1;
		}
		else if (next_val == children[mid]->val) {
			return children.begin() + mid;
		}
		else {
			low = mid + 1;
		}
	}
	//如果没有找到，return NULL;
	return children.end();
}
/*同上，迭代器的封装*/
nodeLoc TreeManager::_loc_nevigate(clsNode* parent, intLoc next_val) {
	return _loc_nevigate(parent, *next_val);
}
/*同上，解引用的封装，如果失败返回NULL*/
clsNode* TreeManager::_nevigate(clsNode* parent, int next_val) {
	nodeLoc destination = _loc_nevigate(parent, next_val);
	if (destination == parent->child_nodes.end()) {
		return NULL;
	}
	else {
		return *destination;
	}
}
/*同上，迭代器的封装*/
clsNode* TreeManager::_nevigate(clsNode* parent, intLoc next_val) {
	return _nevigate(parent, *next_val);
}
/*把两个迭代器中间的数字插入branch以下*/
void TreeManager::_insert_path(clsNode* parent_node, intLoc r_begin, intLoc r_end) {
	intLoc i = r_begin;
	clsNode* cur_node = parent_node;
	clsNode* next_node = NULL;
	//查找前缀
	while (i != r_end) {
		next_node = _nevigate(cur_node, *i);
		if (next_node) {
			cur_node = next_node;
			++i;
		}
		else {
			break;
		}
	}
	//在该层查找要插入的位置
	if (i != r_end && !cur_node->child_nodes.empty()) {
		nodeLoc pos = _locate_branch(cur_node->child_nodes, *i);
		next_node = _create_node(cur_node, *i);
		cur_node->child_nodes.insert(pos, next_node);
		cur_node = next_node;
		++i;
	}
	while (i != r_end){
		next_node = _create_node(cur_node, *i);
		cur_node->child_nodes.push_back(next_node);
		cur_node = next_node;
		++i;
	}
}
/*删除从node开始所有下层的树结构,node不做处理*/
void TreeManager::_destory(clsNode* node) {
	nodeLoc i = node->child_nodes.begin();
	for (; i != node->child_nodes.end(); ++i) {
		if ((*i)->child_nodes.empty()) {
			delete *i;
		}
		else {
			_destory(*i);
			(*i)->child_nodes.clear();
			delete *i;
		}
	}
}
/*递归遍历所有的叶子,把叶子的tempLink链接到负节点*/
void TreeManager::_multiply(clsNode* start_node) {
	if (start_node->child_nodes.empty()) {
		start_node->temp_link = _n_node;
	}
	else {
		nodeLoc i = start_node->child_nodes.begin();
		for (; i != start_node->child_nodes.end(); ++i){
			_multiply(*i);
		}
	}
	return;
}
/*检查收集到的路径中的重复和相反变量*/
//TODO:做一些优化
bool TreeManager::_check(const vector<int>& path) {
	_start = clock();
	_material.clear();
	intLoc i = path.begin();
	_material.push_back(*(i++));
	while (i != path.end()) {
		vector<int>::iterator j = _material.begin();
		while (j != _material.end()) { //找到_material中第一个大于*i的值
			if (*j <= *i) {
				++j;
			}
			else {
				break;
			}
		}
		if (j != _material.begin() && *i == *(j - 1)) { // 检查到重复，舍弃
			++i;
		}
		else {
			if (*i % 2) { //对于奇数检查之前是否有相反变量
				if (j != _material.begin() && *(j - 1) == *i - 1) {
					_stop = clock();
					_check_time += (_stop - _start);
					return false;
				}
				else {
					_material.insert(j, *i);
					++i;
				}
			}
			else { //对于偶数检查之后是否有反变量
				if (j != _material.end() && *j == *i + 1) {
					_stop = clock();
					_check_time += (_stop - _start);
					return false;
				}
				else {
					_material.insert(j, *i);
					++i;
				}
			}
		}
	}
	_stop = clock();
	_check_time += (_stop - _start);
	return true;
}
/*递归中收集新产生的句子*/
void TreeManager::_record(clsNode* cur_node) {
	if (cur_node->child_nodes.empty()) {
		if (cur_node->temp_link == NULL) {
			//深度优先遍历终止，生成语句
			_path.push_back(cur_node->val);
			if (_check(_path)) {
				_start = clock();
				_result_cls.push_back(sCls(_material, TRANS_VAL));
				_stop = clock();
				_g_cls_time += (_stop - _start);
			}
			_path.pop_back();
			return;
		}
		else {
			//p_node终止，接下来是n_node,除了下一个节点走templink以外其他跟普通节点相同
			_path.push_back(cur_node->val);
			clsNode* next_node = cur_node->temp_link;
			nodeLoc i = next_node->child_nodes.begin();
			while (i != next_node->child_nodes.end()){
				_record(*i);
				++i;
			}
			_path.pop_back();
			return;
		}
	}
	else {
		//一般的node
		_path.push_back(cur_node->val);
		nodeLoc i = cur_node->child_nodes.begin();
		while (i != cur_node->child_nodes.end()){
			_record(*i);
			++i;
		}
		//在返回上层之前弹出本层的值
		_path.pop_back();
		return;
	}
}
/*插入树的逻辑*/
void TreeManager::_insert_clause(const sCls& s_cls) {
	_insert_path(_root, s_cls.literals().begin(), s_cls.literals().end());
}

void TreeManager::_insert_path(sCls& s_cls, clsNode* start_point) {
	intLoc i = s_cls.literals().begin();
	clsNode* cur_node = NULL;
	clsNode* last_node = start_point;
	while (i != s_cls.literals().end()) {
		if (*i == start_point->val) {
			break;
		}
		++i;
	}
	assert(start_point->child_nodes.empty());
	++i;
	while (i != s_cls.literals().end()) {
		cur_node = _create_node(last_node, *i);
		last_node->child_nodes.push_back(cur_node);
		last_node = cur_node;
		++i;
	}
}
//检查前缀，不需要插入时返回NULL
clsNode* TreeManager::_prefix(sCls& s_cls) {
	clsNode *cur_node = _root;
	clsNode *last_node = NULL;
	nodeLoc insert_pos;
	intLoc i = s_cls.literals().begin();
	while (i != s_cls.literals().end()) {
		last_node = cur_node;//更新last_node
		cur_node = _nevigate(cur_node, *i);
		if (cur_node) {//找到了当前节点
			if (cur_node->child_nodes.empty()) {//待插入的语句前缀已经存在，被蕴含，舍弃
				return NULL;
			}
			else {//准备下一次查找
				++i;
			}
		}
		else {//没找到，前缀匹配失败
			break;
		}
	}
	//根据挑出循环的原因进行判断
	if (i == s_cls.literals().end()) { //待插入的是短枝或相同
		if (cur_node->child_nodes.empty()) {//相同
			return NULL;
		}
		_destory(cur_node); //待插入的是短枝
		cur_node->child_nodes.clear();
		return NULL;
	}
	insert_pos = _locate_branch(last_node->child_nodes, *i); //查找插入的位置
	cur_node = _create_node(last_node, *i);
	last_node->child_nodes.insert(insert_pos, cur_node);
	return cur_node;
}
/*把当前的情况反馈回database*/
void TreeManager::_retrieve(vector<sCls>& container, clsNode* node){
	if (node->child_nodes.empty()) {
		_path.push_back(node->val);
		container.push_back(sCls(_path, TRANS_VAL));
		_path.pop_back();
		return;
	}
	else {
		if (node->is_root) {
			nodeLoc n = node->child_nodes.begin();
			while (n != node->child_nodes.end()) {
				_retrieve(container, *n);
				++n;
			}
		}
		else {
			_path.push_back(node->val);
			nodeLoc n = node->child_nodes.begin();
			while (n != node->child_nodes.end()) {
				_retrieve(container, *n);
				++n;
			}
			_path.pop_back();
		}
	}
}
/*统计叶子的数量*/
void TreeManager::_count_cls(clsNode* root) {
	if (root->child_nodes.empty()) {
		_cls_count++;
	}
	else {
		vector<clsNode*>::iterator i = root->child_nodes.begin();
		while (i != root->child_nodes.end()) {
			_count_cls(*i);
			++i;
		}
	}
}
/*初始化树*/
void TreeManager::init_tree() {
	sClsLoc i = cls_manager.s_cls().begin();
	for (; i != cls_manager.s_cls().end(); ++i) {
		_insert_path(_root, (*i).literals().begin(), (*i).literals().end());
	}
	return;
}
/*摘出要处理的树，分离相关变量的操作*/
bool TreeManager::build() {
	_reset();
	int index = _root->child_nodes.back()->val;
	if (index % 2){
		//没有对应的正变量分支，直接删除即可,build不成功
		cout << "[WARNING] No matched positive branch found!" << endl;
		_destory(_root->child_nodes.back());
		delete _root->child_nodes.back();
		_root->child_nodes.pop_back();
		return false;
	}
	else {
		//确定正变量的分支
		_p_node = _root->child_nodes.back();
		_root->child_nodes.pop_back();
	}
	++index;
	if (_root->child_nodes.back()->val == index) {
		//负分支符合要求
		_n_node = _root->child_nodes.back();
		_root->child_nodes.pop_back();
	}
	else {
		//没有检测到对应的负分支，直接删除正分支,build不成功
		cout << "[WARNING] No matched negative branch found!" << endl;
		_destory(_p_node);
		delete _p_node;
		return false;
	}
	return true;
}
/*相乘操作*/
void TreeManager::multiply() {
	_multiply(_p_node);
	return;
}
/*处理新句子*/
//TODO:优化插入逻辑
void TreeManager::collect() {
	//搜集
	nodeLoc i = _p_node->child_nodes.begin();
	while (i != _p_node->child_nodes.end()) {
		_record(*i);
		++i;
	}
	cout << "[INFO] " << _result_cls.size() << " clauses generated!" << endl;
	//加入原来的树
	int add_count = 0;
	sClsLoc j = _result_cls.begin();
	while (j != _result_cls.end()) {
		clsNode* insert_point = _prefix(*j);
		if (insert_point) {
			add_count++;
			_insert_path(*j, insert_point);
		}
		++j;
	}
	cout << "[INFO] " << add_count << " clauses add into the tree!" << endl;
	return;
}
/*更新数据库*/
void TreeManager::retrieve() {
	cls_manager.s_cls().clear();
	_path.clear();
	_retrieve(cls_manager.s_cls(), _root);
}
/*统计句子数量*/
int TreeManager::count_clause() {
	_count_cls(_root);
	return _cls_count;
}

TreeManager::~TreeManager(){
	_destory(_root);
	delete _root;
}