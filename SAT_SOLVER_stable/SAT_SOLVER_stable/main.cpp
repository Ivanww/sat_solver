#include <iostream>
#include <fstream>
#include <string>

#include "satSolver.h"

using namespace std;

int main(int argc, char* argv[]) {
	if (argc < 2) {
		cout << "[ERROR] Invalid argument number" << endl;
		cout << "[INFO] SAT-solver stopped" << endl;
		system("Pause");
		return 1;
	}
	ofstream log_file("log.txt", ios::out);
	if (!log_file) {
		cerr << "[ERROR] Can not open log file!" << endl;
		exit(1);
	}
	satSolver ss(argv[1]);
	if (ss.initial()) {
		ss.solve();
	}
	system("Pause");
	return 0;
}