#include <iostream>
#include "singleton.h"

using namespace std;

int main() {
	mySingleton* s = mySingleton::get_instance();

	cout << s->get_data() << endl;
	
	system("Pause");
	return 0;
}