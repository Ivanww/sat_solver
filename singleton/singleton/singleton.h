#ifndef __SINGLETON__
#define __SINGLETON__
#include <iostream>

using namespace std;

class mySingleton{
private:
	static mySingleton* instance;
	int data;

	mySingleton();
public:
	static mySingleton* get_instance() {
		if (instance == NULL) {
			instance = new mySingleton();
		}
		return instance;
	}
	int get_data();
};

mySingleton::mySingleton(){
	data = 0;
}

mySingleton* mySingleton::instance = NULL;

int mySingleton::get_data() {
	return data;
}
#endif