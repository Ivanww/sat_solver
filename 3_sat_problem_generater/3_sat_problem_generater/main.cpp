#include <iostream>
#include <string>
#include <fstream>
#include <time.h>
#include <ctime>
#include <stdlib.h>

using namespace std;

int main() {
	int var_num, cls_num;
	string filename;
	time_t now;
	bool is_repeating = false;
	int vars[3];
	cout << "[INFO] input the var num" << endl;
	cin >> var_num;
	cout << "[INFO] input the cls num" << endl;
	cin >> cls_num;
	cout << "[INFO] input the filename" << endl;
	cin >> filename;
	now = time(NULL);
	//string s_time((int)now);
	//string s_cnf = ".cnf";
	//filename = s_time + s_cnf;

	ofstream cnf(filename, ios::out);
	if (!cnf) {
		cout << "[ERROR] cannot create cnf file." << endl;
		system("Pause");
		return 1;
	}
	cnf << "c 3-SAT Problem Generater 0.1" << endl;
	cnf << "c File name : " << ctime(&now);
	cnf << "p cnf " << var_num << " " << cls_num << endl;

	srand((unsigned)time(NULL));
	for (int i = 0; i != cls_num; ++i) {
		vars[0] = 0;
		for (int j = 0; j != 3; j++) {
			int temp = rand() % (var_num) + 1;
			if (rand() % 2){
				temp = -temp;
			}
			for (int m = 0; m != j; m++) {
				if (vars[m] == temp || vars[m] == -temp) {
					j--;
					is_repeating = true;
					break;
				}
				is_repeating = false;
			}
			if (is_repeating) {
				continue;
			}
			vars[j] = temp;
			cnf << temp << " ";
		}
		cnf << "0" << endl;
	}

	cout << "[INFO] Generating sucessfully" << endl;
	cnf.close();
	system("Pause");
}