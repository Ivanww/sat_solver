#ifndef __SAT_SOLVER__
#define __SAT_SOLVER__
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>

#include "ClauseManager.h"
#include "FileManager.h"
#include "FS_Handler.h"

using namespace std;

class satSolver {
private:
	ofstream _log_os;
	FileManager _file_manager;
	ClauseManager& _cls_manager;
	FS_Handler _fs_handler;

	int _step_num;
	bool _is_log_ok;
	void _proceed();

public:

	satSolver(const string&);
	bool initial();
	void solve();
	~satSolver();
};

#endif