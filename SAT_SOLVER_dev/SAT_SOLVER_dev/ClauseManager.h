#ifndef __CLAUSE_MANAGER__
#define __CLAUSE_MANAGER__
#include <iostream>
#include <vector>
#include <time.h>

#include "BinClause.h"
#include "Clause.h"
#include "MuitiSegBinClause.h"

using namespace std;

class BinGroup {
public:
	int group_id;
	vector<BinClause> positive;
	vector<BinClause> negative;

	BinGroup(int i):group_id(i) {}
};

class SegBinClsGroup {
public:
	int group_id;
	vector<MSBinCls> positive;
	vector<MSBinCls> negative;

	SegBinClsGroup(int i) : group_id(i) {}
};
typedef class RunningStatus {
public :
	int step;
	int stage;
	int literal_num;
	int clause_num;
	int bin_cls_num;
	int seg_cls_num;
	RunningStatus() : step(0), literal_num(0), clause_num(0), \
		bin_cls_num(0), seg_cls_num(0), stage(0){}
} RS;
class ClauseManager {
private:
	RS _rs;
	vector<Cls> _cls_database;
	vector<BinGroup*> _bin_clause_database;
	vector<SegBinClsGroup*> _seg_cls_database;
	/*读取和设定属性*/
	void _count_bin_cls();
	void _count_seg_cls();
	ClauseManager();
public:
	static ClauseManager& get_instance();

	/*读取和设定属性*/
	void literal_num(int l_num) { _rs.literal_num = l_num; } //设定变量个数
	int literal_num() { return _rs.literal_num; } //读取变量个数
	void clause_num(int c_num) { _rs.clause_num = c_num; } //设定语句个数
	int clause_num() { return _rs.clause_num; } //读取语句个数
	int seg_cls_num() { return _rs.seg_cls_num; }
	int remain_cls_num() { return _rs.bin_cls_num; } //剩余语句个数
	bool empty_bin_cls() { return _rs.bin_cls_num == 0; } //是否空剩余语句个数
	int step() { return _rs.step; } //正在消除的变量
	int stage() { return _rs.stage; }
	void update_rs(); //更新RS状态
	void update_bin_cls();
	void update_seg_cls();
	void update_step();

	/*数据操作*/
	void add_cls(const Cls&); //增加语句
	void add_cls(const BinClause&);
	void add_cls(const MSBinCls&);
	void delete_cls(int, bool, int);
	void delete_seg_cls(int, bool, int);
	void init_bin_data();
	/*查询操作*/
	BinClause& find(int, bool, int);
	MSBinCls& find_seg_cls(int, bool, int);
	BinClause null_cls() { return _bin_clause_database[0]->positive[0]; } //返回一个空语句
	BinGroup* group(int group_id) { return _bin_clause_database[group_id]; } //根据GROUPID返回查询结果
	SegBinClsGroup* seg_bin_group(int group_id) { return _seg_cls_database[group_id]; }
	~ClauseManager();
};

#endif // !__CLAUSE_MANAGER__
