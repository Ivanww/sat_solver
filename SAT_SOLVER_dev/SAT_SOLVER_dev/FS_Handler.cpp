#include "FS_Handler.h"

FS_Handler::FS_Handler() : _cls_manager(ClauseManager::get_instance()) { 
	_reset_status();
}

bool FS_Handler::_is_implicated(const BinClause& s, const BinClause& d) {
	//bitset<MAX_LITERAL_NUM> test_result = s.bin_literals() & (~d.bin_literals());
	bitset<MAX_LITERAL_NUM> test_result = ~(~s.bin_signs() & ~s.bin_literals() | s.bin_signs() & d.bin_signs() \
		| ~s.bin_signs() & ~d.bin_signs() & d.bin_literals());
	if (test_result.any()) {
		return false;
	}
	else {
		return true;
	}
}
IMP_TYPE FS_Handler::_relation(const BinClause& a, const BinClause& b) {
	if (a.length() > b.length()) {
		start = clock();
		if (_is_implicated(b, a)) {
			stop = clock();
			timer1 += (stop - start);
			return IMPLICATED;
		}
		else {
			stop = clock();
			timer1 += (stop - start);
			return NOT_IMPLICATE;
		}
	}
	else if (a.length() < b.length()) {
		start = clock();
		if (_is_implicated(a, b)) {
			stop = clock();
			timer1 += (stop - start);
			return IMPLICATE;
		}
		else {
			stop = clock();
			timer1 += (stop - start);
			return NOT_IMPLICATE;
		}

	}
	else {
		start = clock();
		bitset<MAX_LITERAL_NUM> test_lit = a.bin_literals() ^ b.bin_literals();
		if (test_lit.any()) {
			stop = clock();
			timer2 += (stop - start);
			return NOT_IMPLICATE;
		}
		else {
			bitset<MAX_LITERAL_NUM> test_sign = a.bin_signs() ^ b.bin_signs();
			if (test_sign.count() == 1) {
				stop = clock();
				timer2 += (stop - start);
				return SAME_LITERALS;
			}
			else if (test_sign.count() == 0) {
				stop = clock();
				timer2 += (stop - start);
				return SAME_CLAUSE;
			}
			else {
				stop = clock();
				timer2 += (stop - start);
				return NOT_IMPLICATE;
			}
		}
	}
}
bool FS_Handler::_check_status() {
	if (_status.cur_branch) {
		if (_status.cur_loc >= _cls_manager.group(_status.cur_group)->positive.size()) {
			return false;
		}
		else {
			return true;
		}
	}
	else {
		if (_status.cur_loc >= _cls_manager.group(_status.cur_group)->negative.size()) {
			return false;
		}
		else {
			return true;
		}
	}
}
bool FS_Handler::_check_seg_status() {
	if (_status.cur_branch) {
		if (_status.cur_loc >= _cls_manager.seg_bin_group(_status.cur_group)->positive.size()) {
			return false;
		}
		else {
			return true;
		}
	}
	else {
		if (_status.cur_loc >= _cls_manager.seg_bin_group(_status.cur_group)->negative.size()) {
			return false;
		}
		else {
			return true;
		}
	}
}
void FS_Handler::_update_status() {
	_status.relation = UNKNOWN;
	_status.cur_loc++;
	while (true) {
		if (_status.cur_group > _cls_manager.literal_num()) {
			break;
		}
		if (_status.cur_branch) {
			if (_status.cur_loc >= _cls_manager.group(_status.cur_group)->positive.size()) {
				_status.cur_loc = 0;
				_status.cur_branch = false;
			}
			else {
				break;
			}
		}
		if (!_status.cur_branch) {
			if (_status.cur_loc >= _cls_manager.group(_status.cur_group)->negative.size()) {
				_status.cur_loc = 0;
				_status.cur_branch = true;
				_status.cur_group++;
			}
			else {
				break;
			}
		}
	}
}
void FS_Handler::_reset_status() {
	_status.cur_group = _cls_manager.step() + 1;
	_status.cur_branch = true;
	_status.cur_loc = 0;
	_status.has_imp = false;
	_status.has_same = false;
	_status.relation = UNKNOWN;
}
void FS_Handler::_update_seg_status() {
	_status.relation = UNKNOWN;
	_status.cur_loc++;
	while (true) {
		if (_status.cur_group > _cls_manager.literal_num()) {
			break;
		}
		if (_status.cur_branch) {
			if (_status.cur_loc >= _cls_manager.seg_bin_group(_status.cur_group)->positive.size()) {
				_status.cur_loc = 0;
				_status.cur_branch = false;
			}
			else {
				break;
			}
		}
		if (!_status.cur_branch) {
			if (_status.cur_loc >= _cls_manager.seg_bin_group(_status.cur_group)->negative.size()) {
				_status.cur_loc = 0;
				_status.cur_branch = true;
				_status.cur_group++;
			}
			else {
				break;
			}
		}
	}
}
bool FS_Handler::_handle_new_cls(const BinClause& bin_cls) {
	BinClause cur_cls(bin_cls);
	_reset_status();
	while (_status.cur_group <= _cls_manager.literal_num() && !_check_status()) {
		_update_status();
	}
	while (_status.cur_group <= _cls_manager.literal_num()) {
		BinClause& temp = _cls_manager.find(_status.cur_group, \
			_status.cur_branch, _status.cur_loc);
		start = clock();
		_status.relation = _relation(cur_cls, temp);
		stop = clock();
		timer1 += (stop - start);
		start = clock();
		switch (_status.relation) {
		case SAME_CLAUSE: {
							  if (_status.has_same) {
								  //cout << "[DELETE] ";
								  //temp.show(cout);
								  //cout << "\t[BY SAME] ";
								  //cur_cls.show(cout);
								  _delete_cls(_status.cur_group, _status.cur_branch, \
									  _status.cur_loc);
								  if (!_check_status()) {
									  _update_status();
								  }
							  }
							  else {
								  //cout << "[SAME] ";
								  //temp.show(cout);
								  _status.has_same = true;
								  _update_status();
							  }
							  break;
		}
		case IMPLICATED: {
							 //cout << "[IGNORE] ";
							 //cur_cls.show(cout);
							 //cout << "\t[BY]";
							 //temp.show(cout);
							 stop = clock();
							 timer3 += (stop - start);
							 return true;;
		}
		case IMPLICATE: {
							if (_status.has_imp) {
								//cout << "[DELETE] ";
								//temp.show(cout);
								//cout << "\t[BY IMPLICATE] ";
								//cur_cls.show(cout);
								_delete_cls(_status.cur_group, _status.cur_branch, \
									_status.cur_loc);
								if (!_check_status()) {
									_update_status();
								}
							}
							else {
								//cout << "[IMPLICATE] ";
								//temp.show(cout);
								//cout << "\t[BY] ";
								//cur_cls.show(cout);
								_delete_cls(_status.cur_group, _status.cur_branch, \
									_status.cur_loc);
								_insert_cls(cur_cls);
								if (!_check_status()) {
									_update_status();
								}
								_status.has_imp = true;
							}
							break;
		}
		case NOT_IMPLICATE: {
								_update_status();
								break;
		}
		case SAME_LITERALS: {
								if (cur_cls.length() == 1 && temp.length() == 1) {
									cout << "[ABORT] ";
									cur_cls.show(cout);
									cout << "[ABORT] ";
									temp.show(cout);
									stop = clock();
									timer3 += (stop - start);
									return false;
								}
								bitset<MAX_LITERAL_NUM> sign = cur_cls.bin_signs() & temp.bin_signs();
								bitset<MAX_LITERAL_NUM> literals = ~cur_cls.bin_signs() & cur_cls.bin_literals() \
									& ~temp.bin_signs() | sign;
								BinClause new_cls(literals, sign);
								if (new_cls.length()) {
									//cout << "[ADD] ";
									//new_cls.show(cout);
									//cout << "\t[BY SAME LITERALS] ";
									//cur_cls.show(cout);
									//cout << "\t[BY SAME LITERALS] ";
									//temp.show(cout);
									//cout << "\t[AND DELETE] ";
									//temp.show(cout);
									_delete_cls(_status.cur_group, _status.cur_branch, _status.cur_loc);
									_reset_status();
									if (!_check_status()) {
										_update_status();
									}
									_cls_manager.add_cls(new_cls);
									//cout << "\t[RESET STATUS TO] ";
									cur_cls = new_cls;
									//cur_cls.show(cout);
								}
								else {
									//cout << "[EMPTY CLAUSE GENERATED]";
									//cout << "\t[AND DELETE] ";
									//temp.show(cout);
									_delete_cls(_status.cur_group, _status.cur_branch, _status.cur_loc);
									if (!_check_status()) {
										_update_status();
									}
								}
								break;
		}
		default:
			break;
		}
	}
	if (!_status.has_same && !_status.has_imp) {
		//cout << "[ADD] ";
		//cur_cls.show(cout);
		_insert_cls(cur_cls);
	}
	stop = clock();
	timer3 += (stop - start);
	return true;
}
bool FS_Handler::_handle_new_cls(const MSBinCls& seg_cls) {
	MSBinCls cur_cls(seg_cls);
	_reset_status();
	while (_status.cur_group <= _cls_manager.literal_num() && !_check_seg_status()) {
		_update_seg_status();
	}
	while (_status.cur_group <= _cls_manager.literal_num()) {
		MSBinCls& temp = _cls_manager.find_seg_cls(_status.cur_group, \
			_status.cur_branch, _status.cur_loc);
		start = clock();
		_status.relation = cur_cls.relation(temp, _cls_manager.stage());
		stop = clock();
		timer1 += (stop - start);
		start = clock();
		switch (_status.relation) {
		case SAME_CLAUSE: {
							  if (_status.has_same) {
								  //cout << "[DELETE] ";
								  //temp.show(cout);
								  //cout << "\t[BY SAME] ";
								  //cur_cls.show(cout);
								  _delete_seg_cls(_status.cur_group, _status.cur_branch, \
									  _status.cur_loc);
								  if (!_check_seg_status()) {
									  _update_seg_status();
								  }
							  }
							  else {
								  //cout << "[SAME] ";
								  //temp.show(cout);
								  _status.has_same = true;
								  _update_seg_status();
							  }
							  break;
		}
		case IMPLICATED: {
							 //cout << "[IGNORE] ";
							 //cur_cls.show(cout);
							 //cout << "\t[BY]";
							 //temp.show(cout);
							 stop = clock();
							 timer3 += (stop - start);
							 return true;;
		}
		case IMPLICATE: {
							if (_status.has_imp) {
								//cout << "[DELETE] ";
								//temp.show(cout);
								//cout << "\t[BY IMPLICATE] ";
								//cur_cls.show(cout);
								_delete_seg_cls(_status.cur_group, _status.cur_branch, \
									_status.cur_loc);
								if (!_check_seg_status()) {
									_update_seg_status();
								}
							}
							else {
								//cout << "[IMPLICATE] ";
								//temp.show(cout);
								//cout << "\t[BY] ";
								//cur_cls.show(cout);
								_delete_seg_cls(_status.cur_group, _status.cur_branch, \
									_status.cur_loc);
								_insert_cls(cur_cls);
								if (!_check_seg_status()) {
									_update_seg_status();
								}
								_status.has_imp = true;
							}
							break;
		}
		case NOT_IMPLICATE: {
								_update_seg_status();
								break;
		}
		case SAME_LITERALS: {
								if (cur_cls.length() == 1 && temp.length() == 1) {
									cout << "[ABORT] ";
									cur_cls.show(cout);
									cout << "[ABORT] ";
									temp.show(cout);
									stop = clock();
									timer3 += (stop - start);
									return false;
								}
								bitset<MAX_LITERAL_NUM> sign = cur_cls.bin_signs() & temp.bin_signs();
								bitset<MAX_LITERAL_NUM> literals = ~cur_cls.bin_signs() & cur_cls.bin_literals() \
									& ~temp.bin_signs() | sign;
								MSBinCls new_cls(literals, sign);
								if (new_cls.length()) {
									//cout << "[ADD] ";
									//new_cls.show(cout);
									//cout << "\t[BY SAME LITERALS] ";
									//cur_cls.show(cout);
									//cout << "\t[BY SAME LITERALS] ";
									//temp.show(cout);
									//cout << "\t[AND DELETE] ";
									//temp.show(cout);
									_delete_seg_cls(_status.cur_group, _status.cur_branch, _status.cur_loc);
									_reset_status();
									if (!_check_seg_status()) {
										_update_seg_status();
									}
									_insert_cls(new_cls);
									//cout << "\t[RESET STATUS TO] ";
									cur_cls = new_cls;
									//cur_cls.show(cout);
								}
								break;
		}
		default:
			break;
		}
	}
	if (!_status.has_same && !_status.has_imp) {
		//cout << "[ADD] ";
		//cur_cls.show(cout);
		_insert_cls(cur_cls);
	}
	stop = clock();
	timer3 += (stop - start);
	return true;
}
void FS_Handler::_insert_cls(const BinClause& bin_cls) {
	_cls_manager.add_cls(bin_cls);

}
void FS_Handler::_insert_cls(const MSBinCls& seg_cls) {
	_cls_manager.add_cls(seg_cls);
}
void FS_Handler::_delete_cls(int groupid, bool imp_sign, int imp_loc) {
	_cls_manager.delete_cls(groupid, imp_sign, imp_loc);
}
void FS_Handler::_delete_seg_cls(int id, bool branch, int loc){
	_cls_manager.delete_seg_cls(id, branch, loc);
}
bool FS_Handler::do_fs(int step) {
	timer1 = 0;
	timer2 = 0;
	timer3 = 0;
	cout << "[STEP" << _cls_manager.step() << "] " << "Clause Number : " << \
		_cls_manager.remain_cls_num() << endl;
	/*得到GROUPID的所有语句*/
	BinGroup* bg = _cls_manager.group(step);
	/*收集相乘的结果*/
	for (size_t i = 0; i < bg->positive.size(); ++i) {
		assert(bg->positive[i].literals()[0] == step);
		bg->positive[i].split();//去除首字母（只二进制的）
	}
	for (size_t j = 0; j < bg->negative.size(); ++j) {
		assert(bg->negative[j].literals()[0] == -step);
		bg->negative[j].split();
	}
	for (size_t i = 0; i < bg->positive.size(); ++i) {
		for (size_t j = 0; j < bg->negative.size(); ++j) {
			start = clock();
			BinClause result = bg->positive[i] * bg->negative[j];
			stop = clock();
			timer2 += (stop - start);
			//插入原来的树
			if (result.length()) {
				cout << "[STEP " << _cls_manager.step() << " NEW CLAUSE] ";
				result.show(cout);
				if (!_handle_new_cls(result)) {
					return false;
				}
			}
		}
	}
	cout << "[TIME] [RT\tMT\tST]" << endl;
	cout << "[TIME] [" << timer1 << "\t" << timer2 << "\t" << timer3 << "]" << endl;
	return true;
}
bool FS_Handler::do_fs_by_seg(int step) {
	timer1 = 0;
	timer2 = 0;
	timer3 = 0;
	cout << "[STEP" << _cls_manager.step() << "] " << "Clause Number : " << \
		_cls_manager.seg_cls_num() << endl;
	SegBinClsGroup* g = _cls_manager.seg_bin_group(step);
	for (size_t i = 0; i < g->positive.size(); ++i) {
		assert(g->positive[i].literals()[0] == step);
		g->positive[i].split();//去除首字母（只二进制的）
	}
	for (size_t j = 0; j < g->negative.size(); ++j) {
		assert(g->negative[j].literals()[0] == -step);
		g->negative[j].split();
	}
	for (size_t i = 0; i < g->positive.size(); ++i) {
		for (size_t j = 0; j < g->negative.size(); ++j) {
			start = clock();
			MSBinCls result = g->positive[i] * g->negative[j];
			stop = clock();
			timer2 += (stop - start);
			//插入原来的树
			if (result.length()) {
				//cout << "[STEP " << _cls_manager.step() << " NEW CLAUSE] ";
				//result.show(cout);
				if (!_handle_new_cls(result)) {
					return false;
				}
			}
		}
	}
	cout << "[TIME] [RT\tMT\tST]" << endl;
	cout << "[TIME] [" << timer1 << "\t" << timer2 << "\t" << timer3 << "]" << endl;
	return true;
}
FS_Handler::~FS_Handler() {

}