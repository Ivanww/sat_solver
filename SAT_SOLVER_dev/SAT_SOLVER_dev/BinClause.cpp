#include "BinClause.h"

BinClause::BinClause() : Cls() {
	_group_id = 0;
	_group_sign = false;
}
BinClause::BinClause(const Cls& cls) : Cls(cls) {
	assert(_length < MAX_LITERAL_NUM);
	_group_id = _abs(_literals.front());
	for each (int i in _literals) {
		if (i > 0) {
			_bin_literals.set(i);
			_bin_signs.set(i);
		}
		else {
			_bin_literals.set(_abs(i));
		}
		if (_abs(i) < _group_id) {
			_group_id = _abs(i);
		}
	}
	_translate();//调整字母的顺序
	assert(_group_id = _abs(_literals[0]));
	_group_sign = _bin_signs.test(_group_id);
}
BinClause::BinClause(const vector<int>& literals) : Cls() {
	_group_id = _abs(literals[0]);
	assert(_length <= MAX_LITERAL_NUM);
	for each (int i in literals) {
		if (i > 0) {
			_bin_literals.set(i);
			_bin_signs.set(i);
		}
		else {
			_bin_literals.set(_abs(i));
		}
		if (_abs(i) < _group_id) {
			_group_id = _abs(i);
		}
	}
	_group_sign = _bin_signs.test(_group_id);
	_translate();
	_length = _literals.size();
}
BinClause::BinClause(int* literal_array, int length) : Cls(){
	_group_id = _abs(literal_array[0]);
	_length = length;
	assert(_length <= MAX_LITERAL_NUM);
	for (int pos = 0; pos < length; ++pos) {
		_literals.push_back(literal_array[pos]);
		if (literal_array[pos] > 0) {
			_bin_literals.set(literal_array[pos]);
			_bin_signs.set(literal_array[pos]);
		}
		else {
			_bin_literals.set(_abs(literal_array[pos]));
		}
		if (_abs(literal_array[pos]) < _group_id) {
			_group_id = _abs(literal_array[pos]);
		}
	}
	_group_sign = _bin_signs.test(_group_id);
}
BinClause::BinClause(const bitset<MAX_LITERAL_NUM>& bin_literals, const bitset<MAX_LITERAL_NUM>& bin_signs) : Cls() {
	_bin_literals = bin_literals;
	_bin_signs = bin_signs;
	_translate();
	_length = _literals.size();
	if (_length) {
		_group_id = _abs(_literals.front());
		_group_sign = _bin_signs.test(_group_id);
	}
	else {
		_group_id = 0;
		_group_sign = false;
	}
}
BinClause::BinClause(const BinClause& bin_cls) {
	_literals = bin_cls.literals();
	_bin_literals = bin_cls.bin_literals();
	_bin_signs = bin_cls.bin_signs();
	_group_id = bin_cls.groupid();
	_group_sign = bin_cls.groupsign();
	_length = bin_cls.length();
}
void BinClause::_translate() {
	_literals.clear();
	int i = 1;
	while (i < MAX_LITERAL_NUM) {
		if (_bin_literals.test(i)) {
			if (_bin_signs.test(i)) {
				_literals.push_back(i);
			}
			else {
				_literals.push_back(-i);
			}
		}
		++i;
	}
}
void BinClause::erase(int i) {
	_bin_literals.reset(i);
	_bin_signs.reset(i);
	_length--;
	if (i == _group_id) {
		while (i < MAX_LITERAL_NUM) {
			if (_bin_literals.test(i)) {
				_group_id = i;
				break;
			}
			++i;
		}
	}
}
void BinClause::split() {
	erase(_group_id);
}
void BinClause::update(const bitset<MAX_LITERAL_NUM>& literals, const bitset<MAX_LITERAL_NUM>& signs){
	_bin_literals = literals;
	_bin_signs = signs;
	_translate();
	_length = _literals.size();
	if (_length) {
		_group_id = _abs(_literals.front());
	}
	else {
		_group_id = 0;
	}
}
void BinClause::show(ostream& os = cout) const{
	os << "[Detail] ";
	for each (int i in _literals){
		os << i << "\t";
	}
	os << endl;
}

BinClause BinClause::operator*(const BinClause& op) {
	if (!_length && !op.length()) {
		return BinClause();
	}
	bitset<MAX_LITERAL_NUM> opposite = (_bin_signs ^ op.bin_signs()) & _bin_literals & op.bin_literals();
	if (opposite.any()) {
		return BinClause();//有相反变量，返回空语句
	}
	else {
		return BinClause(_bin_literals | op.bin_literals(), _bin_signs | op.bin_signs());
	}
}
BinClause::~BinClause() {
}
