#include "ClauseManager.h"


ClauseManager::ClauseManager(){
}

//获取数据库的引用
ClauseManager& ClauseManager::get_instance() {
	static ClauseManager instance;
	return instance;
}

/*读取和设定属性*/
void ClauseManager::_count_bin_cls() {
	_rs.bin_cls_num = 0;
	for (int i = _rs.step+1; i <= _rs.literal_num; ++i) {
		_rs.bin_cls_num += _bin_clause_database[i]->positive.size();
		_rs.bin_cls_num += _bin_clause_database[i]->negative.size();
	}
}
void ClauseManager::_count_seg_cls() {
	_rs.seg_cls_num = 0;
	for (int i = _rs.step + 1; i <= _rs.literal_num; ++i) {
		_rs.seg_cls_num += _seg_cls_database[i]->positive.size();
		_rs.seg_cls_num += _seg_cls_database[i]->negative.size();
	}
}
void ClauseManager::update_rs() {
	_rs.step++;
	_count_bin_cls();
}
void ClauseManager::update_bin_cls() {
	_count_bin_cls();
}
void ClauseManager::update_seg_cls() {
	_count_seg_cls();
}
void ClauseManager::update_step() {
	_rs.step++;
	_rs.stage = _rs.step / (MAX_LITERAL_NUM / SEGMENT_NUM);
}
/*数据操作*/
void ClauseManager::init_bin_data() {
	_bin_clause_database.push_back(new BinGroup(0));
	_bin_clause_database[0]->positive.push_back(BinClause());
	for (int group_id = 1; group_id <= _rs.literal_num; ++group_id) {
		_bin_clause_database.push_back(new BinGroup(group_id));
	}

	_seg_cls_database.push_back(new SegBinClsGroup(0));
	for (int group_id = 1; group_id <= _rs.literal_num; ++group_id) {
		_seg_cls_database.push_back(new SegBinClsGroup(group_id));
	}
}
void ClauseManager::add_cls(const Cls& cls) {
	_cls_database.push_back(cls);
	add_cls(BinClause(cls));
	add_cls(MSBinCls(cls));
	return;
}
void ClauseManager::add_cls(const BinClause& bin_cls) {
	BinGroup* bin_group = _bin_clause_database[bin_cls.groupid()];
	if (bin_cls.groupsign()) {
		bin_group->positive.push_back(bin_cls);
	}
	else {
		bin_group->negative.push_back(bin_cls);
	}
}
void ClauseManager::add_cls(const MSBinCls& ms_cls) {
	SegBinClsGroup* group = _seg_cls_database[ms_cls.groupid()];
	if (ms_cls.groupsign()) {
		group->positive.push_back(ms_cls);
	}
	else {
		group->negative.push_back(ms_cls);
	}
}
void ClauseManager::delete_cls(int groupid, bool branch, int loc) {
	BinGroup* bg = _bin_clause_database[groupid];
	if (branch) {
		bg->positive.erase(bg->positive.begin() + loc);
	}
	else {
		bg->negative.erase(bg->negative.begin() + loc);
	}
}
void ClauseManager::delete_seg_cls(int id, bool branch, int loc){
	SegBinClsGroup* g = _seg_cls_database[id];
	if (branch) {
		g->positive.erase(g->positive.begin() + loc);
	}
	else {
		g->negative.erase(g->negative.begin() + loc);
	}
}
/*查询操作*/
BinClause& ClauseManager::find(int group_id, bool branch, int loc) {
	if (branch) {
		return _bin_clause_database[group_id]->positive[loc];
	}
	else {
		return _bin_clause_database[group_id]->negative[loc];
	}
}
MSBinCls& ClauseManager::find_seg_cls(int group_id, bool branch, int loc) {
	if (branch) {
		return _seg_cls_database[group_id]->positive[loc];
	}
	else {
		return _seg_cls_database[group_id]->negative[loc];
	}
}

ClauseManager::~ClauseManager(){
	for (size_t group_id = 0; group_id < _bin_clause_database.size(); ++group_id) {
		delete _bin_clause_database[group_id];
	}
	for (size_t id = 0; id < _seg_cls_database.size(); ++id){
		delete _seg_cls_database[id];
	}
}