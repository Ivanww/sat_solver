#include "FileManager.h"

FileManager::FileManager(const string& filename) :_cls_manager(ClauseManager::get_instance()){
	_stat_info.file_name = filename;
	_stat_info.clause_num = 0;
	_stat_info.var_num = 0;
	_stat_info.is_cls_checked = false;
	_stat_info.is_var_checked = false;
	_var_count = NULL;
}

int FileManager::_abs(int v) {
	return v >= 0 ? v : -v;
}

void FileManager::_read_cnf(const string& filename) {
	char lineBuffer[MAX_LINE_LENGTH];
	char wordBuffer[MAX_WORD_LENGTH];
	vector<int> vars;
	int line_count = 0;
	int clause_num = 0;
	int var_num = 0;

	ifstream cnfFile(filename, ios::in);

	if (!cnfFile) {
		cerr << "[ERROR] Can not open file " << filename << endl;
		return;
	}

	while (cnfFile.getline(lineBuffer, MAX_LINE_LENGTH)) {
		line_count++;
		if (lineBuffer[0] == 'c') {
			cout << "[INFO]" << lineBuffer[1] << endl;
			continue;
		}
		else if (lineBuffer[0] == 'p') {
			int arg = sscanf(lineBuffer, "p cnf %d %d", &(_stat_info.var_num), &(_stat_info.clause_num));
			if (arg < 2) {
				cerr << "[ERROR] Format error in " << filename << " line " << line_count << endl;
				return;
			}
			//统计变量数目
			_var_count = new int[_stat_info.var_num + 1];
			fill(&_var_count[0], &_var_count[_stat_info.var_num + 1], 0);
			_cls_manager.literal_num(_stat_info.var_num);
			_cls_manager.clause_num(_stat_info.clause_num);
			_cls_manager.init_bin_data();
		}
		else {
			char* pLine = lineBuffer;
			while (*pLine) {
				char* pWord = wordBuffer;
				//过滤掉空白
				while (*pLine && (*pLine == ' ' || *pLine == '\t')) {
					++pLine;
				}
				//空白之间的内容
				while (*pLine && (*pLine != ' ') && (*pLine != '\t') && (*pLine != '\n')) {
					*(pWord++) = *(pLine++);
				}
				//使word成为一个字符串
				*pWord = '\0';

				if (strlen(wordBuffer)) {
					//如果有数字被捕获
					int var = atoi(wordBuffer);
					_var_count[_abs(var)]++;
					if (var) {
						vars.push_back(var);
					}
					else {
						//clause temp_clause(vars);
						//BinClause bin_clause(vars);
						_cls_manager.add_cls(clause(vars));
						//_cls_manager.add_cls(bin_clause);
						vars.clear();
					}
				}
			}
		}
	}
	cnfFile.close();
	cout << "[INFO] " << line_count << " lines in total!" << endl;
}

void FileManager::_check_literals() {
	//检查情况
	for (int i = 1; i <= _stat_info.var_num; i++) {
		if (_var_count[i] <= 0) {
			return;
		}
	}
	_stat_info.is_var_checked = true;
}

void FileManager::_check_clauses() {
	if (_cls_manager.clause_num() == _stat_info.clause_num) {
		_stat_info.is_cls_checked = true;
	}
}

void FileManager::dump_vars(ostream& os = cout) const{
	os << "Var\tCount\tVar\tCount\tVar\tCount" << endl;
	for (int i = 1; i < _stat_info.var_num + 1; i++) {
		os << i << " :\t" << _var_count[i] << "\t";
		if (!(i % 5)) {
			os << endl;
		}
	}
	os << endl;
	return;
}

void FileManager::_report_cnf(ostream& os) const{
	os << "\t\t====== CNF file Report ======" << endl;
	os << "[INFO] Total Variable : " << _stat_info.var_num << endl;
	os << "[INFO] Total Clause : " << _stat_info.clause_num << endl;
	if (_stat_info.is_var_checked) {
		os << "[SUCCESS] Variables number checking successful" << endl;
	}
	else {
		os << "[WARNING] Variables number checking unsuccessful" << endl;
		os << "[WARNING] Some variables may missing" << endl;
	}
	if (_stat_info.is_cls_checked) {
		os << "[SUCCESS] Clause number checking successful" << endl;
	}
	else {
		os << "[WARNING] Clause number checking unsuccessful" << endl;
		os << "[WARNING] Some clauses may missing" << endl;
	}
	dump_vars(os);
	os << "\t\t====== CNF file Report End ======" << endl;
}

bool FileManager::initial(ostream& log = cout) {
	if (_stat_info.file_name == "") {
		cout << "[ERROR] File name missing! " << endl;
		return false;
	}
	else {
		_read_cnf(_stat_info.file_name);
		_cls_manager.update_bin_cls();
		_cls_manager.update_seg_cls();
		_check_literals();
		//_cls_manager.literal_num(_stat_info.var_num);
		_check_clauses();
		_cls_manager.clause_num(_stat_info.clause_num);
		_report_cnf(log);
		return true;
	}
}

void FileManager::dump_bin_cls(ostream& os = cout) const {
	int temp_cls_count = 0;
	os << "[INFO] This is Step : " << _cls_manager.step() << endl;
	os << "[INFO] Remaining Clause Number : " << _cls_manager.remain_cls_num() << endl;
	os << "[INFO] ====== \tDump Clause Data \t ======" << endl;
	//从下一个变量开始记录
	for (int group_id = _cls_manager.step() + 1; group_id <= _cls_manager.literal_num(); ++group_id) {
		os << "[GROUPID] " << group_id << endl;
		BinGroup* bg = _cls_manager.group(group_id);
		for each (BinClause cls in bg->positive) {
			temp_cls_count++;
			os << "[CLAUSE " << temp_cls_count << "]\t";
			for each (int i in cls.literals()) {
				os << i << "\t";
			}
			os << endl;
		}
		for each (BinClause cls in bg->negative) {
			temp_cls_count++;
			os << "[CLAUSE " << temp_cls_count << "]\t";
			for each (int i in cls.literals()) {
				os << i << "\t";
			}
			os << endl;
		}
	}
}
FileManager::~FileManager(){
	delete _var_count;
}