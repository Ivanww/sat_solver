#ifndef __FS_HANDLER__
#define __FS_HANDLER__

#include <iostream>
#include <time.h>
#include "ClauseManager.h"

using namespace std;

struct status {
	int cur_group;
	size_t cur_loc;
	bool cur_branch;
	IMP_TYPE relation;
	bool has_same;
	bool has_imp;
};
class FS_Handler {
private:
	status _status;
	ClauseManager& _cls_manager;

	time_t start, stop;

	time_t timer1, timer2, timer3;

	bool _is_implicated(const BinClause&, const BinClause&);
	void _insert_cls(const BinClause&);
	void _insert_cls(const MSBinCls&);
	IMP_TYPE _relation(const BinClause&, const BinClause&);
	void _delete_cls(int, bool, int);
	void _delete_seg_cls(int, bool, int);
	bool _handle_new_cls(const BinClause&);
	bool _handle_new_cls(const MSBinCls&);
	void _update_status();
	void _update_seg_status();
	void _reset_status();
	bool _check_status();
	bool _check_seg_status();
	
public:
	FS_Handler();
	~FS_Handler();
	bool do_fs(int);
	bool do_fs_by_seg(int);
	
};
#endif