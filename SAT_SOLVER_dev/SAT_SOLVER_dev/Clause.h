#ifndef __CLAUSE__
#define __CLAUSE__

#include <iostream>
#include <vector>

using namespace std;

class clause {
protected:
	int _length;
	vector<int> _literals; //����

public:
	clause();
	clause(const vector<int>&);
	clause(const clause&);
	const vector<int>& literals() const;
	int length() const;

	~clause();
};

typedef clause Cls;

#endif // !__CLAUSE__