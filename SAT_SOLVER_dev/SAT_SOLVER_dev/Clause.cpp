#include "clause.h"

clause::clause():_length(0){
}

clause::clause(const vector<int>& vars) : _literals(vars) {
	_length = vars.size();
}

clause::clause(const clause& cls) {
	_literals = cls.literals();
	_length = cls.length();
}

int clause::length() const{
	return _length;
}

const vector<int>& clause::literals() const{
	return _literals;
}

clause ::~clause(){
}