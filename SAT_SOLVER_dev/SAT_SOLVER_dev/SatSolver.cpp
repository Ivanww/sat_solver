#include "satSolver.h"

satSolver::satSolver(const string& file_name) :_cls_manager(ClauseManager::get_instance()), _file_manager(file_name), _step_num(0){
	_log_os.open("log.txt", ios::out);
	if (!_log_os) {
		cout << "[WARNING] Cannot open log file! Log will be send to STDOUT" << endl;
		return;
	}
	_is_log_ok = true;
	return;
}

bool satSolver::initial() {
	if (_file_manager.initial(cout)) {
		_file_manager.dump_bin_cls(_log_os);
		return true;
	}
	else {
		cout << "[ERROR] cnfManager initializing failed! " << endl;
		return false;
	}
}

void satSolver::solve() {
	while (_cls_manager.remain_cls_num()) {
		_cls_manager.update_step();
		if (_cls_manager.step() > _cls_manager.literal_num()) {
			break;
		}
		if (!_fs_handler.do_fs(_cls_manager.step())){
			cout << "UNSAT" << endl;
			return;
		}
		_cls_manager.update_bin_cls();
		//if (!_fs_handler.do_fs_by_seg(_cls_manager.step())) {
		//	cout << "UNSAT" << endl;
		//	return;
		//}
		//_cls_manager.update_seg_cls();
		//_file_manager.dump_bin_cls(_log_os);
		
	}
	cout << "SAT" << endl;
	return;
}

satSolver ::~satSolver() {
	if (_is_log_ok) {
		_log_os.close();
	}
}