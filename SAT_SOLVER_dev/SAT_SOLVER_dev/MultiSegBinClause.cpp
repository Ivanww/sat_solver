#include "MuitiSegBinClause.h"

MSBinCls::MultiSegBinClause() : BinClause() {

}
MSBinCls::MultiSegBinClause(const Cls& cls) : BinClause(cls) {
	_dispatch();
}
MSBinCls::MultiSegBinClause(const vector<int>& literals) : BinClause(literals) {
	_dispatch();
}
MSBinCls::MultiSegBinClause(int *int_array, int length) : BinClause(int_array, length) {
	_dispatch();
}
MSBinCls::MultiSegBinClause(const bitset<MAX_LITERAL_NUM>& bin_literal, const bitset<MAX_LITERAL_NUM>&bin_sign) : BinClause(bin_literal, bin_sign) {
	_dispatch();
}
MSBinCls::MultiSegBinClause(const BinClause& bin_cls) : BinClause(bin_cls) {
	_dispatch();
}
void MSBinCls::_dispatch() {
	for each (int  i in _literals) {
		int seg = _abs(i) / (MAX_LITERAL_NUM / SEGMENT_NUM);
		int loc = _abs(i) % (MAX_LITERAL_NUM / SEGMENT_NUM);
		if (i > 0) {
			_seg_literals[seg].set(loc);
			_seg_signs[seg].set(loc);
		}
		else {
			_seg_literals[seg].set(loc);
		}
	}
}
void MSBinCls::_reset(int literal){
	int segment = _abs(literal) / (MAX_LITERAL_NUM/ SEGMENT_NUM);
	int loc = _abs(literal) % (MAX_LITERAL_NUM / SEGMENT_NUM);
	_seg_literals[segment].reset(loc);
	_seg_signs[segment].reset(loc);
}
IMP_TYPE MSBinCls::_is_implicate(const MSBinCls& op, int start_seg) {
	if (_length > op.length()) {
		for (int i = start_seg; i < SEGMENT_NUM; ++i) {
			bitset<MAX_LITERAL_NUM / SEGMENT_NUM> test_result = ~(~op.seg_literals(i) & ~op.seg_signs(i) \
				| op.seg_signs(i) & _seg_signs[i] \
				| ~op.seg_signs(i) & ~_seg_signs[i] & _seg_literals[i]);
			if (test_result.any()) {
				return NOT_IMPLICATE;
			}
		}
		return IMPLICATED;
	}
	else {
		for (int i = start_seg; i < SEGMENT_NUM; ++i) {
			bitset<MAX_LITERAL_NUM / SEGMENT_NUM> test_result = ~(~_seg_literals[i] & ~_seg_signs[i] \
				| _seg_signs[i] & op.seg_signs(i) \
				| ~_seg_signs[i] & ~op.seg_signs(i) & op.seg_literals(i));
			if (test_result.any()) {
				return NOT_IMPLICATE;
			}
		}
		return IMPLICATE;
	}
}
void MSBinCls::split() {
	BinClause::split();
	_reset(_literals[0]);
}
IMP_TYPE MSBinCls::relation(const MSBinCls& op, int start_seg) {
	if (_length == op.length()) {
		int dif_sign = 0;
		bitset<MAX_LITERAL_NUM / SEGMENT_NUM> test;
		for (int i = start_seg; i < SEGMENT_NUM; ++i){
			//先测试字母出现是否相同
			test = _seg_literals[i] ^ op.seg_literals(i);
			if (test.any()) {
				return NOT_IMPLICATE;
			}
			//再测试符号是否相同
			test = _seg_signs[i] ^ op.seg_signs(i);
			dif_sign += test.count();
			if (dif_sign > 1) {
				return NOT_IMPLICATE;
			}
		}
		if (dif_sign) {
			return SAME_LITERALS;
		}
		else {
			return SAME_CLAUSE;
		}
	}
	else {
		return _is_implicate(op, start_seg);
	}
}

MSBinCls MSBinCls::operator*(const MSBinCls& op) {
	if (!_length && !op.length()) {
		return MSBinCls();
	}
	for (int seg = 0; seg < SEGMENT_NUM; ++seg) {
		bitset<MAX_LITERAL_NUM / SEGMENT_NUM> oppo = (_seg_signs[seg] ^ op.seg_signs(seg)) \
			& _seg_literals[seg] & op.seg_literals(seg);
		if (oppo.any()) {
			return MSBinCls();
		}
	}
	return MSBinCls(this->bin_literals() | op.bin_literals(), this->bin_signs() | op.bin_signs());
}